<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class Api extends Model
{     
    // const URL_BTSONAIR = 'http://10.54.36.49/api-btsonair/public/';
    const URL_CHANGE = 'http://10.54.36.49/change2/public/'; 
    const URL_BTSONAIR = 'http://localhost/api-approval/public/';
    // const URL_CHANGE = 'http://localhost/change2/public/';           
    
    static function dataNodin() 
    {
        $url = self::URL_BTSONAIR.'api/table_list_nodin';
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi dataNodin");
        }

        return json_decode($res->getBody(), true);
    }

    static function dataMom()
    {
        $url = self::URL_BTSONAIR.'api/table_list_mom';
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi dataMom");
        }

        return json_decode($res->getBody(), true);
    } 

    static function dataBtsSummary1($date)
    {
        $url = self::URL_BTSONAIR.'api/bts_summary_1?date='.$date;
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi dataBtsSummary1");
        } 

        return json_decode($res->getBody(), true);
    }

    static function dataBtsSummary2($date)
    {
        $url = self::URL_BTSONAIR.'api/bts_summary_2_monthly1?date='.$date;
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi dataBtsSummary2");
        } 

        return json_decode($res->getBody(), true);
    }

    static function dataBtsOnAir() 
    {
        $url = self::URL_BTSONAIR.'api/bts_summary_2';
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi dataBtsOnAir");
        } 

        return json_decode($res->getBody(), true);
    }

    static function dataBtsOnAirMonthly() 
    {
        $url = self::URL_BTSONAIR.'api/bts_summary_2_monthly';
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi dataBtsOnAirMonthly");
        } 

        return json_decode($res->getBody(), true);
    }

    static function dataResumeSite()
    {
        $url = self::URL_BTSONAIR.'api/resume_site';
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi dataResumeSite");
        } 

        return json_decode($res->getBody(), true);
    }

    static function dataResumeSiteMonthly()
    {
        $url = self::URL_BTSONAIR.'api/resume_site_monthly';
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi dataResumeSiteMonthly");
        } 

        return json_decode($res->getBody(), true);
    }

    static function dataResumeNe()
    {
        $url = self::URL_BTSONAIR.'api/resume_ne';
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi dataResumeNe");
        } 

        return json_decode($res->getBody(), true);
    } 

    static function dataResumeNeMonthly()
    {
        $url = self::URL_BTSONAIR.'api/resume_ne_monthly';
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi dataResumeNeMonthly");
        } 

        return json_decode($res->getBody(), true);
    }

    static function dataResumeNodinBts()
    {
        $url = self::URL_BTSONAIR.'api/bts_summary_1';
        $client = new \GuzzleHttp\Client(); 
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi dataResumeNodinBts");
        } 

        return json_decode($res->getBody(), true);
    }

    static function dataResumeNodinBtsMonthly()
    {    
        $url = self::URL_BTSONAIR.'api/bts_summary_1';
        $client = new \GuzzleHttp\Client(); 
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi dataResumeNodinBtsMonthly");
        } 

        return json_decode($res->getBody(), true);
    } 
    
    static function onairBts4g($limit, $offset, $search)
    {  
        $url = self::URL_BTSONAIR.'api/onair_bts_4g?limit='.$limit.'&skip='.$offset.'&search='.$search;
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi onairBts4g");
        }  
         
        return json_decode($res->getBody(), true);
    }

    static function onairBts3g($limit, $offset, $search)
    {  
        $url = self::URL_BTSONAIR.'api/onair_bts_3g?limit='.$limit.'&skip='.$offset.'&search='.$search;
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi onairBts3g");
        }  
         
        return json_decode($res->getBody(), true);
    }

    static function onairBts2g($limit, $offset, $search)
    {   
        $url = self::URL_BTSONAIR.'api/onair_bts_2g?limit='.$limit.'&skip='.$offset.'&search='.$search;
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi onairBts2g");
        }  
         
        return json_decode($res->getBody(), true);
    }

    static function getonairBts4g($regional_user) 
    {  
        $url = self::URL_BTSONAIR.'api/onair_bts_4g_all?regional='.$regional_user;
        $client = new \GuzzleHttp\Client();
        try {    
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi getonairBts4g");
        }  
        ini_set('memory_limit', '-1');
        return json_decode($res->getBody(), true);
    }

    static function getonairBts3g($regional_user) 
    {  
        $url = self::URL_BTSONAIR.'api/onair_bts_3g_all?regional='.$regional_user;
        $client = new \GuzzleHttp\Client();
        try {  
            $res = $client->request('GET', $url); 
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi getonairBts3g");
        }  
        ini_set('memory_limit', '-1');
        return json_decode($res->getBody(), true); 
    }
 
    static function getonairBts2g($regional_user) 
    {    
        $url = self::URL_BTSONAIR.'api/onair_bts_2g_all?regional='.$regional_user;
        $client = new \GuzzleHttp\Client();       
        try {   
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi getonairBts2g");
        }  
        ini_set('memory_limit', '-1');
        return json_decode($res->getBody(), true);
    }

    static function dataChart2g()
    {
        $url = self::URL_CHANGE.'btsonair4g';
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi dataChart2g");
        }  
         
        return json_decode($res->getBody(), true);
    }

    static function dataChart3g()
    {
        $url = self::URL_CHANGE.'btsonair4g';
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi dataChart3g");
        }  
         
        return json_decode($res->getBody(), true);
    }

    
    static function dataChart4g()
    {
        $url = self::URL_CHANGE.'btsonair4g';
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi dataChart4g");
        }  
         
        return json_decode($res->getBody(), true);
    }

    static function sysinfo4g($data)
    {   
        $url = self::URL_BTSONAIR.'api/sysinfo4g'; 
        $client = new \GuzzleHttp\Client();  

        try { 
            $res = $client->request('POST', $url, [
                'form_params' => $data
            ]);    

        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd($e->getMessage());
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi sysinfo4g");
        }  
          
        return json_decode($res->getBody(), true);
    }

    static function sysinfo3g($data)
    {   
        $url = self::URL_BTSONAIR.'api/sysinfo3g'; 
        $client = new \GuzzleHttp\Client();  

        try { 
            $res = $client->request('POST', $url, [
                'form_params' => $data
            ]);    

        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd($e->getMessage());
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi sysinfo3g");
        }  
          
        return json_decode($res->getBody(), true);
    }
    
    static function sysinfo2g($data)
    {   
        $url = self::URL_BTSONAIR.'api/sysinfo2g';  
        $client = new \GuzzleHttp\Client();  

        try { 
            $res = $client->request('POST', $url, [
                'form_params' => $data
            ]);    

        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd($e->getMessage());
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi sysinfo2g");
        }   
          
        return json_decode($res->getBody(), true);
    }  

    static function deleteData()
    {
        $url = self::URL_CHANGE.'deleteData';
        $client = new \GuzzleHttp\Client();
        try {
            $res = $client->request('GET', $url);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            dd("Terjadi Kesalahan Pada Koneksi Server. Silahkan Dicoba Lagi deleteData");
        }  
         
        return json_decode($res->getBody(), true);
    }   
    

}
