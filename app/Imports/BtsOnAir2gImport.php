<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Api;

class BtsOnAir2gImport implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) 
        {     
            $data = array(
                'REGIONAL' => $row[0],
                'AREA' => $row[1],
                'VENDOR' => $row[2],
                'BSC_NAME' => $row[3],
                'SITE_NAME' => $row[4],
                'CELL_NAME' => $row[5],
                'NEID' => $row[6], 
                'SITEID' => $row[7],
                'LAC' => $row[8],
                'CELL_ID' => $row[9],
                'BTSNUMBER' => $row[10],
                'CELLNUMBER' => $row[11],
                'FREQUENCY' => $row[12],
                'BANDTYPE' => $row[13],
                'NCC' => $row[14], 
                'BCC' => $row[15],
                'f0' => $row[16],
                'f1' => $row[17],
                'f2' => $row[18],
                'f3' => $row[19],
                'f4' => $row[20],
                'f5' => $row[21], 
                'f6' => $row[22],
                'f7' => $row[23],
                'f8' => $row[24],
                'f9' => $row[25],
                'f10' => $row[26],
                'f11' => $row[27],
                'Trx_Sec' => $row[28],
                'Trx_Site' => $row[29],
                'JMLH_SITE' => $row[30],
                'JMLH_BTS' => $row[31],
                'JMLH_CELL' => $row[32],
                'JMLH_BSC' => $row[33],
                'METRO_E' => $row[34],
                'OWNER_LINK' => $row[35],
                'TIPE_LINK' => $row[36],
                'FAR_END_LINK' => $row[37],
                'TOTAL_BANDWITH' => $row[38],
                'TANGGAL_ONAIR_LEASE_LINE' => $row[39],
                'SITE_SIMPUL' => $row[40],
                'JUMLAH_SITE_UNDER_SIMPUL' => $row[41],
                'STATUS_LOKASI' => $row[42],
                'CLUSTER_SALES' => $row[43],
                'TYPE_BTS' => $row[44],
                'STATUS' => $row[45],
                'TYPE_FREQ' => $row[46],
                'NEW_EXISTING' => $row[47],                        
                'ONAIR' => $row[48],
                'DATE_ONAIR' => $row[49],
                'KPI_PASS' => $row[50],
                'DATE_KPI_PASS' => $row[51],
                'REMARK' => $row[52],
                'DEPARTEMENT' => $row[53],
                'TECHNICAL_AREA' => $row[54],
                'ALAMAT' => $row[55],
                'KELURAHAN' => $row[56],
                'KECAMATAN' => $row[57],
                'KABUPATEN_KOTA' => $row[58],
                'PROVINSI' => $row[59],
                'TOWER_PROVIDER' => $row[60],
                'NAMA_TOWER_PROVIDER' => $row[61],
                'STATUS_PLN' => $row[62],
                'VENDOR_FMC' => $row[63],
            );  
 
            if($data['REGIONAL'] == "REGIONAL") {
            } else { 
                Api::sysinfo2g($data); 
            } 
        }
    }
}