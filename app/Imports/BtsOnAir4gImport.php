<?php

namespace App\Imports;
 
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Api;

class BtsOnAir4gImport implements ToCollection
{
    /** 
    * @param Collection $collection
    */  

    public function collection(Collection $rows)
    {  
        foreach ($rows as $row) 
        {    
            $data = array(
                'REGIONAL' => $row[0],
                'AREA' => $row[1],
                'VENDOR' => $row[2],
                'ENODEB_NAME' => $row[3],
                'CELL_NAME' => $row[4], 
                'NEID' => $row[5], 
                'SITEID' => $row[6],
                'TAC' => $row[7],
                'ENODEBID' => $row[8],
                'CI' => $row[9],
                'EARNFCN' => $row[10],
                'PID' => $row[11],
                'FREQUENCY' => $row[12],
                'BANDTYPE' => $row[13],
                'BANDWITH' => $row[14],
                'JMLH_ENODEB' => $row[15],
                'JMLH_CELL' => $row[16],
                'METRO_E' => $row[17],
                'OWNER_LINK' => $row[18],
                'TIPE_LINK' => $row[19],
                'FAR_END_LINK' => $row[20],
                'TOTAL_BANDWITH' => $row[21],
                'TANGGAL_ONAIR_LEASE_LINE' => $row[22],
                'SITE_SIMPUL' => $row[23],
                'JUMLAH_SITE_UNDER_SIMPUL' => $row[24],
                'STATUS_LOKASI' => $row[25],
                'CLUSTER_SALES' => $row[26],
                'TYPE_BTS' => $row[27],
                'STATUS' => $row[28],
                'NEW_EXISTING' => $row[29],
                'ONAIR' => $row[30],
                'DATE_ONAIR' => $row[31],
                'KPI_PASS' => $row[32],
                'DATE_KPI_PASS' => $row[33],
                'REMARK' => $row[34],
                'DEPARTEMENT' => $row[35],
                'TECHNICAL_AREA' => $row[36],
                'LONGITUDE' => $row[37],
                'LATITUDE' => $row[38],
                'ALAMAT' => $row[39],
                'KELURAHAN' => $row[40],
                'KECAMATAN' => $row[41],
                'KABUPATEN' => $row[42],
                'PROVINSI' => $row[43],
                'TOWER_PROVIDER' => $row[44],
                'NAMA_TOWER_PROVIDER' => $row[45],
                'STATUS_PLN' => $row[46],
                'VENDOR_FMC' => $row[47],
                'STATUS_BTS' => 'UNKNOWN STYLO'
            );

            if($data['REGIONAL'] == "REGIONAL") {
            } else { 
                Api::sysinfo4g($data); 
            }
        } 
    }
}
