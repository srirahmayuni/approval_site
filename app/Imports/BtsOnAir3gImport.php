<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Api;

class BtsOnAir3gImport implements ToCollection
{
    /**
    * @param Collection $collection
    */ 
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) 
        {     
            $data = array(
                'REGIONAL' => $row[0],
                'AREA' => $row[1],
                'VENDOR' => $row[2],
                'RNC_NAME' => $row[3], 
                'NODEB_NAME' => $row[4],
                'CELL_NAME' => $row[5],
                'NEID' => $row[6],
                'SITEID' => $row[7],
                'HSDPA' => $row[8],
                'LAC' => $row[9],
                'CI' => $row[10],
                'BTSNUMBER' => $row[11],
                'CELLNUMBER' => $row[12],
                'SAC' => $row[13],
                'PScCode' => $row[14],
                'BANDTYPE' => $row[15],
                'FREQUENCY' => $row[16],
                'FREQ' => $row[17], 
                'F1_F2_F3' => $row[18],
                'JMLH_RNC' => $row[19],
                'JMLH_NODEB' => $row[20],
                'JMLH_CELL' => $row[21],
                'METRO_E' => $row[22],
                'OWNER_LINK' => $row[23],
                'TIPE_LINK' => $row[24],
                'FAR_END_LINK' => $row[25],
                'TOTAL_BANDWITH' => $row[26],
                'TANGGAL_ONAIR_LEASE_LINE' => $row[27],
                'SITE_SIMPUL' => $row[28],
                'JUMLAH_SITE_UNDER_SIMPUL' => $row[29],
                'STATUS_LOKASI' => $row[30],
                'CLUSTER_SALES' => $row[31],
                'TYPE_BTS' => $row[32],
                'STATUS' => $row[33],
                'NEW_EXISTING' => $row[34],
                'ONAIR' => $row[35],
                'DATE_ONAIR' => $row[36],
                'KPI_PASS' => $row[37],
                'DATE_KPI_PASS' => $row[38],
                'REMARK' => $row[39],
                'DEPARTEMENT' => $row[40],
                'TECHNICAL_AREA' => $row[41],
                'ALAMAT' => $row[42],
                'KELURAHAN' => $row[43],
                'KECAMATAN' => $row[44], 
                'KABUPATEN' => $row[45],
                'PROVINSI' => $row[46],
                'TOWER_PROVIDER' => $row[47],
                'NAMA_TOWER_PROVIDER' => $row[48],
                'STATUS_PLN' => $row[49],
                'VENDOR_FMC' => $row[50],
            ); 
 
            if($data['REGIONAL'] == "REGIONAL") {
            } else { 
                Api::sysinfo3g($data); 
            } 
        } 
    }
}
