<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\Api;
use PDF;

class NodinMomController extends Controller
{
    public function index()
    {
    	return view('view-nodin-mom'); 
    }   
     
    public function getJsonDataNodin(Request $request) 
    {  
        return DataTables::of(Api::dataNodin())
        ->make(true);  
    }

    public function getJsonDataMom(Request $request) 
    {
        return DataTables::of(Api::dataMom())
        ->make(true); 
    }

    public function generatePdfNodin($date)
    {
        $dataNodin = collect(Api::dataNodin())
        ->where('input_date','==',$date);

        $dataBtsSummary1 = Api::dataBtsSummary1($date); 
        $dataBtsSummary2 = Api::dataBtsSummary2($date);     

        $originalDate = $date;
        $formatDate1 = date("d-F-Y", strtotime($originalDate));
        $formatDate2 = date("F-Y", strtotime($originalDate));
        $formatDate3 = date("d F Y", strtotime($originalDate));
        
        $pdf = PDF::loadView('pdfNodin', ['dataMom'=>$dataMom, 'dataBtsSummary1'=>$dataBtsSummary1, 'dataBtsSummary2'=>$dataBtsSummary2, 'formatDate1'=>$formatDate1, 'formatDate2'=>$formatDate2,  'formatDate3'=>$formatDate3]);
        return $pdf->download('dataNodin.pdf'); 
    } 

    public function generatePdfMom($date)
    {                                             
        $dataMom = collect(Api::dataMom())
        ->where('input_date','==',$date);
         
        $dataBtsSummary1 = Api::dataBtsSummary1($date); 
        $dataBtsSummary2 = Api::dataBtsSummary2($date);

        // dd($dataBtsSummary2);  

        $originalDate = $date;
        $formatDate1 = date("d-F-Y", strtotime($originalDate));
        $formatDate2 = date("F-Y", strtotime($originalDate.' -1 month'));
        $formatDate3 = date("d F Y", strtotime($originalDate));
        
        $pdf = PDF::loadView('pdfMom', ['dataMom'=>$dataMom, 'dataBtsSummary1'=>$dataBtsSummary1, 'dataBtsSummary2'=>$dataBtsSummary2, 'formatDate1'=>$formatDate1, 'formatDate2'=>$formatDate2,  'formatDate3'=>$formatDate3]);
        return $pdf->download('dataMom.pdf'); 
    }
}
