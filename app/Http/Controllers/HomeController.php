<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Api;
use App\Imports\BtsOnAir4gImport;
use App\Imports\BtsOnAir3gImport;
use App\Imports\BtsOnAir2gImport;
use App\Export\BtsOnAirExport;
use App\Export\ResumeSiteExport;
use App\Export\ResumeNeExport;
use App\Export\ResumeNodinBtsExport;
use App\Export\BtsOnAirExportMon;
use App\Export\ResumeSiteExportMon; 
use App\Export\ResumeNeExportMon;
use App\Export\ResumNodinBtsExportMon;
use App\Export\TemplateSysinfo4g; 
use App\Export\TemplateSysinfo3g;
use App\Export\TemplateSysinfo2g; 
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use App\Export\BaselineOnAirBts4g;
use App\Export\BaselineOnAirBts3g;
use App\Export\BaselineOnAirBts2g;
use DataTables; 
use App\Charts\MontlyViews;
use DB;
use Carbon\Carbon;
// use App\Jobs\SummaryEnodebJob;

class HomeController extends Controller
{
    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {

        // $this->middleware('auth');
    } 

    /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
      session_start(); 
      $rule_user = $_SESSION["rule_akses"];
      $regional_user = $_SESSION["regional"];
      
      // $rule_user = "fixed";  
      // $regional_user = "REGIONAL3";

      return view('baseline-bts4g', compact('rule_user', 'regional_user'));
    } 

    public function baseline3g()
    {  
      session_start();
      $rule_user = $_SESSION["rule_akses"];
      $regional_user = $_SESSION["regional"];
      
      // $rule_user = "fixed";  
      // $regional_user = "REGIONAL3";

        return view('baseline-bts3g', compact('rule_user', 'regional_user'));
    }

} 