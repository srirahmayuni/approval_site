<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Api;
use
    DataTables\Editor,
    DataTables\Editor\Field,
    DataTables\Editor\Format,
    DataTables\Editor\Mjoin,
    DataTables\Editor\Options,
    DataTables\Editor\Upload,
    DataTables\Editor\Validate,
    DataTables\Editor\ValidateOptions;

class DatatableController extends Controller
{

    public function getJsonDataBts4g(Request $request)
    {
        $columns = array(
            0 => 'REGIONAL',
            1 => 'AREA',
            2 => 'VENDOR',
            3 => 'ENODEB_NAME',
            4 => 'CELL_NAME',
            5 => 'NEID',
            6 => 'SITEID',
            7 => 'TAC',
            8 => 'ENODEBID',
            9 => 'CI',
            10 => 'EARNFCN',
            11 => 'PID',
            12 => 'FREQUENCY',
            13 => 'BANDTYPE',
            14 => 'BANDWITH',
            15 => 'JMLH_ENODEB',
            16 => 'JMLH_CELL',
            17 => 'METRO_E',
            18 => 'OWNER_LINK',
            19 => 'TIPE_LINK',
            20 => 'FAR_END_LINK',
            21 => 'TOTAL_BANDWITH',
            22 => 'TANGGAL_ONAIR_LEASE_LINE',
            23 => 'SITE_SIMPUL',
            24 => 'JUMLAH_SITE_UNDER_SIMPUL',
            25 => 'STATUS_LOKASI',
            26 => 'CLUSTER_SALES',
            27 => 'TYPE_BTS',
            28 => 'STATUS',
            29 => 'NEW_EXISTING',
            30 => 'ONAIR',
            31 => 'DATE_ONAIR',
            32 => 'KPI_PASS',
            33 => 'DATE_KPI_PASS',
            34 => 'REMARK',
            35 => 'DEPARTEMENT',
            36 => 'TECHNICAL_AREA',
            37 => 'LONGITUDE',
            38 => 'LATITUDE',
            39 => 'ALAMAT',
            40 => 'KELURAHAN',
            41 => 'KECAMATAN',
            42 => 'KABUPATEN',
            43 => 'PROVINSI',
            44 => 'TOWER_PROVIDER',
            45 => 'NAMA_TOWER_PROVIDER',
            46 => 'STATUS_PLN',
            47 => 'VENDOR_FMC',
            48 => 'INPUT_DATE',
            49 => 'STATUS_BTS'
        ); 
                            
        $limit = $request->input('length');  
        $offset = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir'); 
        
        if(empty($request->input('search.value'))){ 
            $search = NULL;
            $posts = Api::onairBts4g($limit, $offset, $search, $order, $dir);      
            $totalFiltered = $posts['count']; 
        }else{
            $search = $request->input('search.value');
            $posts = Api::onairBts4g($limit, $offset, $search, $order, $dir);
            $totalFiltered = $posts['count'];  
        }		
         
        $data = array();
        
        if($posts){
            foreach($posts['data'] as $r){
                $nestedData['REGIONAL'] = $r['REGIONAL'];
                $nestedData['AREA'] = $r['AREA'];
                $nestedData['VENDOR'] = $r['VENDOR'];
                $nestedData['ENODEB_NAME'] = $r['ENODEB_NAME'];
                $nestedData['CELL_NAME'] = $r['CELL_NAME'];
                $nestedData['NEID'] = $r['NEID'];
                $nestedData['SITEID'] = $r['SITEID'];
                $nestedData['TAC'] = $r['TAC'];
                $nestedData['ENODEBID'] = $r['ENODEBID'];
                $nestedData['CI'] = $r['CI'];
                $nestedData['EARNFCN'] = $r['EARNFCN'];
                $nestedData['PID'] = $r['PID'];
                $nestedData['FREQUENCY'] = $r['FREQUENCY'];
                $nestedData['BANDTYPE'] = $r['BANDTYPE'];
                $nestedData['BANDWITH'] = $r['BANDWITH'];
                $nestedData['JMLH_ENODEB'] = $r['JMLH_ENODEB'];
                $nestedData['JMLH_CELL'] = $r['JMLH_CELL']; 
                $nestedData['METRO_E'] = $r['METRO_E'];
                $nestedData['OWNER_LINK'] = $r['OWNER_LINK'];
                $nestedData['TIPE_LINK'] = $r['TIPE_LINK'];
                $nestedData['FAR_END_LINK'] = $r['FAR_END_LINK'];
                $nestedData['TOTAL_BANDWITH'] = $r['TOTAL_BANDWITH'];
                $nestedData['TANGGAL_ONAIR_LEASE_LINE'] = $r['TANGGAL_ONAIR_LEASE_LINE'];
                $nestedData['SITE_SIMPUL'] = $r['SITE_SIMPUL'];
                $nestedData['JUMLAH_SITE_UNDER_SIMPUL'] = $r['JUMLAH_SITE_UNDER_SIMPUL'];
                $nestedData['STATUS_LOKASI'] = $r['STATUS_LOKASI'];
                $nestedData['CLUSTER_SALES'] = $r['CLUSTER_SALES'];
                $nestedData['TYPE_BTS'] = $r['TYPE_BTS'];
                $nestedData['STATUS'] = $r['STATUS'];
                $nestedData['NEW_EXISTING'] = $r['NEW_EXISTING'];
                $nestedData['ONAIR'] = $r['ONAIR'];
                $nestedData['DATE_ONAIR'] = $r['DATE_ONAIR'];
                $nestedData['KPI_PASS'] = $r['KPI_PASS'];
                $nestedData['DATE_KPI_PASS'] = $r['DATE_KPI_PASS'];
                $nestedData['REMARK'] = $r['REMARK'];
                $nestedData['DEPARTEMENT'] = $r['DEPARTEMENT'];
                $nestedData['TECHNICAL_AREA'] = $r['TECHNICAL_AREA'];
                $nestedData['LONGITUDE'] = $r['LONGITUDE'];
                $nestedData['LATITUDE'] = $r['LATITUDE'];
                $nestedData['ALAMAT'] = $r['ALAMAT'];
                $nestedData['KELURAHAN'] = $r['KELURAHAN'];
                $nestedData['KECAMATAN'] = $r['KECAMATAN'];
                $nestedData['KABUPATEN'] = $r['KABUPATEN'];
                $nestedData['PROVINSI'] = $r['PROVINSI'];
                $nestedData['TOWER_PROVIDER'] = $r['TOWER_PROVIDER'];
                $nestedData['NAMA_TOWER_PROVIDER'] = $r['NAMA_TOWER_PROVIDER'];
                $nestedData['STATUS_PLN'] = $r['STATUS_PLN'];
                $nestedData['VENDOR_FMC'] = $r['VENDOR_FMC'];
                $nestedData['INPUT_DATE'] = $r['INPUT_DATE'];
                $nestedData['STATUS_BTS'] = $r['STATUS_BTS'];
                $data[] = $nestedData;
            } 
        }

        $json_data = array(
            "draw"			=> intval($request->input('draw')),
            "recordsTotal"	=> intval($posts['count']),
            "recordsFiltered" => intval($totalFiltered),
            "data"			=> $data
        );  
        echo json_encode($json_data);       
    } 

    public function getJsonDataBts3g(Request $request)
    { 
        $columns = array(
            0 => 'REGIONAL',
            1 => 'AREA',
            2 => 'VENDOR',
            3 => 'RNC_NAME',
            4 => 'NODEB_NAME',
            5 => 'CELL_NAME',
            6 => 'NEID',
            7 => 'SITEID',
            8 => 'HSDPA',
            9 => 'LAC',
            10 => 'CI',
            11 => 'BTSNUMBER',
            12 => 'CELLNUMBER', 
            13 => 'SAC',
            14 => 'PScCode',
            15 => 'BANDTYPE',
            16 => 'FREQUENCY',
            17 => 'FREQ',
            18 => 'F1_F2_F3',
            19 => 'JMLH_RNC',
            20 => 'JMLH_NODEB',
            21 => 'JMLH_CELL',
            22 => 'METRO_E',
            23 => 'OWNER_LINK',
            24 => 'TIPE_LINK',
            25 => 'FAR_END_LINK',
            26 => 'TOTAL_BANDWITH',
            27 => 'TANGGAL_ONAIR_LEASE_LINE',
            28 => 'SITE_SIMPUL',
            29 => 'JUMLAH_SITE_UNDER_SIMPUL',
            30 => 'STATUS_LOKASI',
            31 => 'CLUSTER_SALES',
            32 => 'TYPE_BTS',
            33 => 'STATUS',
            34 => 'NEW_EXISTING',
            35 => 'ONAIR',
            36 => 'DATE_ONAIR',
            37 => 'KPI_PASS',
            38 => 'DATE_KPI_PASS',
            39 => 'REMARK',
            40 => 'DEPARTEMENT',
            41 => 'TECHNICAL_AREA',
            42 => 'ALAMAT',
            43 => 'KELURAHAN',
            44 => 'KECAMATAN',
            45 => 'KABUPATEN',
            46 => 'PROVINSI',
            47 => 'TOWER_PROVIDER',
            48 => 'NAMA_TOWER_PROVIDER',
            49 => 'STATUS_PLN',
            50 => 'VENDOR_FMC',
            51 => 'INPUT_DATE',
            52 => 'STATUS_BTS' 
        );  
                            
        $limit = $request->input('length');  
        $offset = $request->input('start');
        
        if(empty($request->input('search.value'))){
            $search = NULL;
            $posts = Api::onairBts3g($limit, $offset, $search);      
            $totalFiltered = $posts['count']; 
        }else{  
            $search = $request->input('search.value');
            $posts = Api::onairBts3g($limit, $offset, $search);
            $totalFiltered = $posts['count'];    
        }		
         
        $data = array(); 
        
        if($posts){
            foreach($posts['data'] as $r){
                $nestedData['REGIONAL'] = $r['REGIONAL'];
                $nestedData['AREA'] = $r['AREA'];
                $nestedData['VENDOR'] = $r['VENDOR'];
                $nestedData['RNC_NAME'] = $r['RNC_NAME'];
                $nestedData['NODEB_NAME'] = $r['NODEB_NAME'];
                $nestedData['CELL_NAME'] = $r['CELL_NAME'];                
                $nestedData['NEID'] = $r['NEID'];
                $nestedData['SITEID'] = $r['SITEID'];
                $nestedData['HSDPA'] = $r['HSDPA'];
                $nestedData['LAC'] = $r['LAC'];
                $nestedData['CI'] = $r['CI'];
                $nestedData['BTSNUMBER'] = $r['BTSNUMBER'];
                $nestedData['CELLNUMBER'] = $r['CELLNUMBER'];
                $nestedData['SAC'] = $r['SAC'];
                $nestedData['PScCode'] = $r['PScCode'];
                $nestedData['BANDTYPE'] = $r['BANDTYPE'];
                $nestedData['FREQUENCY'] = $r['FREQUENCY']; 
                $nestedData['FREQ'] = $r['FREQ'];
                $nestedData['F1_F2_F3'] = $r['F1_F2_F3'];
                $nestedData['JMLH_RNC'] = $r['JMLH_RNC'];
                $nestedData['JMLH_NODEB'] = $r['JMLH_NODEB'];
                $nestedData['JMLH_CELL'] = $r['JMLH_CELL']; 
                $nestedData['METRO_E'] = $r['METRO_E'];
                $nestedData['OWNER_LINK'] = $r['OWNER_LINK'];
                $nestedData['TIPE_LINK'] = $r['TIPE_LINK'];
                $nestedData['FAR_END_LINK'] = $r['FAR_END_LINK'];
                $nestedData['TOTAL_BANDWITH'] = $r['TOTAL_BANDWITH'];
                $nestedData['TANGGAL_ONAIR_LEASE_LINE'] = $r['TANGGAL_ONAIR_LEASE_LINE'];
                $nestedData['SITE_SIMPUL'] = $r['SITE_SIMPUL'];
                $nestedData['JUMLAH_SITE_UNDER_SIMPUL'] = $r['JUMLAH_SITE_UNDER_SIMPUL'];
                $nestedData['STATUS_LOKASI'] = $r['STATUS_LOKASI'];
                $nestedData['CLUSTER_SALES'] = $r['CLUSTER_SALES'];
                $nestedData['TYPE_BTS'] = $r['TYPE_BTS'];
                $nestedData['STATUS'] = $r['STATUS'];
                $nestedData['NEW_EXISTING'] = $r['NEW_EXISTING'];
                $nestedData['ONAIR'] = $r['ONAIR'];
                $nestedData['DATE_ONAIR'] = $r['DATE_ONAIR'];
                $nestedData['KPI_PASS'] = $r['KPI_PASS'];
                $nestedData['DATE_KPI_PASS'] = $r['DATE_KPI_PASS'];
                $nestedData['REMARK'] = $r['REMARK'];
                $nestedData['DEPARTEMENT'] = $r['DEPARTEMENT'];
                $nestedData['TECHNICAL_AREA'] = $r['TECHNICAL_AREA'];
                $nestedData['ALAMAT'] = $r['ALAMAT'];
                $nestedData['KELURAHAN'] = $r['KELURAHAN'];
                $nestedData['KECAMATAN'] = $r['KECAMATAN'];
                $nestedData['KABUPATEN'] = $r['KABUPATEN'];
                $nestedData['PROVINSI'] = $r['PROVINSI'];
                $nestedData['TOWER_PROVIDER'] = $r['TOWER_PROVIDER'];
                $nestedData['NAMA_TOWER_PROVIDER'] = $r['NAMA_TOWER_PROVIDER'];
                $nestedData['STATUS_PLN'] = $r['STATUS_PLN'];
                $nestedData['VENDOR_FMC'] = $r['VENDOR_FMC'];
                $nestedData['INPUT_DATE'] = $r['INPUT_DATE'];
                $nestedData['STATUS_BTS'] = $r['STATUS_BTS'];
                $data[] = $nestedData;
            } 
        }  
           
        $json_data = array(
            "draw"			=> intval($request->input('draw')),
            "recordsTotal"	=> intval($posts['count']),
            "recordsFiltered" => intval($totalFiltered),
            "data"			=> $data
        );  
        echo json_encode($json_data);       
    }

    public function getJsonDataBts2g(Request $request)
    {
        $columns = array(
 `           0 => 'REGIONAL',
            1 => 'AREA',
            2 => 'VENDOR',
            3 => 'BSC_NAME',
            4 => 'SITE_NAME',
            5 => 'CELL_NAME',
            6 => 'NEID',
            7 => 'SITEID',
            8 => 'LAC',
            9 => 'CELL_ID',
            10 => 'BTSNUMBER',
            11 => 'CELLNUMBER',
            12 => 'FREQUENCY',
            13 => 'BANDTYPE',
            14 => 'NCC',
            15 => 'BCC',
            16 => 'f0',
            17 => 'f1',
            18 => 'f2',
            19 => 'f3',
            20 => 'f4',
            21 => 'f5',
            22 => 'f6',
            23 => 'f7',
            24 => 'f8',
            25 => 'f9',
            26 => 'f10',
            27 => 'f11',
            28 => 'Trx_Sec',
            29 => 'Trx_Site',
            30 => 'JMLH_SITE',
            31 => 'JMLH_BTS',
            32 => 'JMLH_CELL',
            33 => 'JMLH_BSC',
            34 => 'METRO_E',
            35 => 'OWNER_LINK',
            36 => 'TIPE_LINK',
            37 => 'FAR_END_LINK',
            38 => 'TOTAL_BANDWITH',
            39 => 'TANGGAL_ONAIR_LEASE_LINE',
            40 => 'SITE_SIMPUL',
            41 => 'JUMLAH_SITE_UNDER_SIMPUL',
            42 => 'STATUS_LOKASI',
            43 => 'CLUSTER_SALES',
            44 => 'TYPE_BTS',
            45 => 'STATUS',
            46 => 'TYPE_FREQ',            
            47 => 'NEW_EXISTING',
            48 => 'ONAIR',
            49 => 'DATE_ONAIR',
            50 => 'KPI_PASS',
            51 => 'DATE_KPI_PASS',
            52 => 'REMARK',
            53 => 'DEPARTEMENT',
            54 => 'TECHNICAL_AREA',
            55 => 'ALAMAT',
            56 => 'KELURAHAN',
            57 => 'KECAMATAN',
            58 => 'KABUPATEN_KOTA',
            59 => 'PROVINSI',
            60 => 'TOWER_PROVIDER',
            61 => 'NAMA_TOWER_PROVIDER',
            62 => 'STATUS_PLN',
            63 => 'VENDOR_FMC',
            64 => 'INPUT_DATE',
            65 => 'STATUS_BTS' `
        );  

        $limit = $request->input('length');  
        $offset = $request->input('start');
        
        if(empty($request->input('search.value'))){
            $search = NULL;  
            $posts = Api::onairBts2g($limit, $offset, $search);      
            $totalFiltered = $posts['count']; 
        }else{
            $search = $request->input('search.value');
            $posts = Api::onairBts2g($limit, $offset, $search); 
            $totalFiltered = count(Api::onairBts2g($limit, $offset, $search));  
        }		
         
        $data = array(); 
        
        if($posts){
            foreach($posts['data'] as $r){
                $nestedData['REGIONAL'] = $r['REGIONAL'];
                $nestedData['AREA'] = $r['AREA'];
                $nestedData['VENDOR'] = $r['VENDOR'];
                $nestedData['BSC_NAME'] = $r['BSC_NAME'];
                $nestedData['SITE_NAME'] = $r['SITE_NAME'];
                $nestedData['CELL_NAME'] = $r['CELL_NAME'];                
                $nestedData['NEID'] = $r['NEID'];
                $nestedData['SITEID'] = $r['SITEID'];
                $nestedData['LAC'] = $r['LAC'];
                $nestedData['CELL_ID'] = $r['CELL_ID'];
                $nestedData['BTSNUMBER'] = $r['BTSNUMBER'];
                $nestedData['CELLNUMBER'] = $r['CELLNUMBER'];
                $nestedData['FREQUENCY'] = $r['FREQUENCY']; 
                $nestedData['BANDTYPE'] = $r['BANDTYPE'];
                $nestedData['NCC'] = $r['NCC'];
                $nestedData['BCC'] = $r['BCC'];
                $nestedData['f0'] = $r['f0'];
                $nestedData['f1'] = $r['f1']; 
                $nestedData['f2'] = $r['f2'];
                $nestedData['f3'] = $r['f3'];
                $nestedData['f4'] = $r['f4'];
                $nestedData['f5'] = $r['f5'];
                $nestedData['f6'] = $r['f6'];
                $nestedData['f7'] = $r['f7'];
                $nestedData['f8'] = $r['f8'];
                $nestedData['f9'] = $r['f9'];
                $nestedData['f10'] = $r['f10'];
                $nestedData['f11'] = $r['f11'];
                $nestedData['Trx_Sec'] = $r['Trx_Sec'];
                $nestedData['Trx_Site'] = $r['Trx_Site'];
                $nestedData['JMLH_SITE'] = $r['JMLH_SITE'];
                $nestedData['JMLH_BTS'] = $r['JMLH_BTS'];
                $nestedData['JMLH_CELL'] = $r['JMLH_CELL'];
                $nestedData['JMLH_BSC'] = $r['JMLH_BSC'];
                $nestedData['METRO_E'] = $r['METRO_E'];
                $nestedData['OWNER_LINK'] = $r['OWNER_LINK'];
                $nestedData['TIPE_LINK'] = $r['TIPE_LINK'];
                $nestedData['FAR_END_LINK'] = $r['FAR_END_LINK'];
                $nestedData['TOTAL_BANDWITH'] = $r['TOTAL_BANDWITH']; 
                $nestedData['TANGGAL_ONAIR_LEASE_LINE'] = $r['TANGGAL_ONAIR_LEASE_LINE'];
                $nestedData['SITE_SIMPUL'] = $r['SITE_SIMPUL'];
                $nestedData['JUMLAH_SITE_UNDER_SIMPUL'] = $r['JUMLAH_SITE_UNDER_SIMPUL'];
                $nestedData['STATUS_LOKASI'] = $r['STATUS_LOKASI'];
                $nestedData['CLUSTER_SALES'] = $r['CLUSTER_SALES'];
                $nestedData['TYPE_BTS'] = $r['TYPE_BTS'];
                $nestedData['STATUS'] = $r['STATUS'];
                $nestedData['TYPE_FREQ'] = $r['TYPE_FREQ'];
                $nestedData['NEW_EXISTING'] = $r['NEW_EXISTING'];
                $nestedData['ONAIR'] = $r['ONAIR'];
                $nestedData['DATE_ONAIR'] = $r['DATE_ONAIR'];
                $nestedData['KPI_PASS'] = $r['KPI_PASS'];
                $nestedData['DATE_KPI_PASS'] = $r['DATE_KPI_PASS'];
                $nestedData['REMARK'] = $r['REMARK'];
                $nestedData['DEPARTEMENT'] = $r['DEPARTEMENT'];
                $nestedData['TECHNICAL_AREA'] = $r['TECHNICAL_AREA'];
                $nestedData['ALAMAT'] = $r['ALAMAT'];
                $nestedData['KELURAHAN'] = $r['KELURAHAN'];
                $nestedData['KECAMATAN'] = $r['KECAMATAN'];
                $nestedData['KABUPATEN_KOTA'] = $r['KABUPATEN_KOTA'];
                $nestedData['PROVINSI'] = $r['PROVINSI'];
                $nestedData['TOWER_PROVIDER'] = $r['TOWER_PROVIDER'];
                $nestedData['NAMA_TOWER_PROVIDER'] = $r['NAMA_TOWER_PROVIDER'];
                $nestedData['STATUS_PLN'] = $r['STATUS_PLN'];
                $nestedData['VENDOR_FMC'] = $r['VENDOR_FMC'];
                $nestedData['INPUT_DATE'] = $r['INPUT_DATE'];
                $nestedData['STATUS_BTS'] = $r['STATUS_BTS'];
                $data[] = $nestedData;
            } 
        }   
           
        $json_data = array(
            "draw"			=> intval($request->input('draw')),
            "recordsTotal"	=> intval($posts['count']),
            "recordsFiltered" => intval($totalFiltered),
            "data"			=> $data
        );  
        echo json_encode($json_data);       
    }

}
