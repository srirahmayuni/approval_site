<?php

namespace App\Export;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class TemplateSysinfo4g implements FromCollection, WithHeadings
{
 
    public function headings(): array
    {
        return [
            'REGIONAL',
            'AREA',
            'VENDOR',
            'ENODEB_NAME',
            'CELL_NAME',
            'NEID',
            'SITEID',
            'TAC',
            'ENODEBID',
            'CI',
            'EARNFCN',
            'PID',
            'FREQUENCY',
            'BANDTYPE',
            'BANDWITH',
            'JMLH_ENODEB',
            'JMLH_CELL',
            'METRO_E', 
            'OWNER_LINK',
            'TIPE_LINK',
            'FAR_END_LINK',
            'TOTAL_BANDWITH',
            'TANGGAL_ONAIR_LEASE_LINE',
            'SITE_SIMPUL',
            'JUMLAH_SITE_UNDER_SIMPUL',
            'STATUS_LOKASI',
            'CLUSTER_SALES',
            'TYPE_BTS',
            'STATUS',
            'NEW_EXISTING',
            'ONAIR',
            'DATE_ONAIR',
            'KPI_PASS',
            'DATE_KPI_PASS',
            'REMARK',
            'DEPARTEMENT',
            'TECHNICAL_AREA',
            'LONGITUDE',
            'LATITUDE',
            'ALAMAT',
            'KELURAHAN',
            'KECAMATAN',
            'KABUPATEN',
            'PROVINSI',
            'TOWER_PROVIDER',
            'NAMA_TOWER_PROVIDER',
            'STATUS_PLN',
            'VENDOR_FMC'
        ]; 
    }   
 
    public function collection()
    { 
        $get = collect([]);
        return $get; 
    }
}
