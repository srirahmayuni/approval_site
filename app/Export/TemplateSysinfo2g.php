<?php

namespace App\Export;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class TemplateSysinfo2g implements FromCollection, WithHeadings
{ 
 
    public function headings(): array
    {
        return [
            'REGIONAL',
            'AREA',
            'VENDOR',
            'BSC_NAME',
            'SITE_NAME',
            'CELL_NAME',
            'NEID',
            'SITEID', 
            'LAC',
            'CELL_ID',
            'BTSNUMBER',
            'CELLNUMBER',
            'FREQUENCY',
            'BANDTYPE',
            'NCC', 
            'BCC',
            'f0',
            'f1',
            'f2', 
            'f3',
            'f4',
            'f5', 
            'f6',
            'f7',
            'f8',
            'f9',
            'f10',
            'f11',
            'Trx_Sec',
            'Trx_Site',
            'JMLH_SITE',
            'JMLH_BTS',
            'JMLH_CELL',
            'JMLH_BSC',
            'METRO_E',
            'OWNER_LINK',
            'TIPE_LINK',
            'FAR_END_LINK',
            'TOTAL_BANDWITH',
            'TANGGAL_ONAIR_LEASE_LINE',
            'SITE_SIMPUL',
            'JUMLAH_SITE_UNDER_SIMPUL',
            'STATUS_LOKASI',
            'CLUSTER_SALES',
            'TYPE_BTS',
            'STATUS',
            'TYPE_FREQ',
            'NEW_EXISTING',                        
            'ONAIR',
            'DATE_ONAIR',
            'KPI_PASS',
            'DATE_KPI_PASS',
            'REMARK',
            'DEPARTEMENT',
            'TECHNICAL_AREA',
            'LONGITUDE',
            'LATITUDE',
            'ALAMAT',
            'KELURAHAN',
            'KECAMATAN',
            'KABUPATEN_KOTA',
            'PROVINSI',
            'TOWER_PROVIDER',
            'NAMA_TOWER_PROVIDER',
            'STATUS_PLN',
            'VENDOR_FMC'
        ];
    }

    public function collection() 
    {
        $get = collect([]);
        return $get;
    }
}
