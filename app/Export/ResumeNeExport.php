<?php

namespace App\Export;
use App\Api;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ResumeNeExport implements FromCollection, WithHeadings
{

    public function headings(): array
    {
        return [
            'regional',
            'vendor',
            '2g_bsc',
            '2g_site',
            '2g_bts',
            '2g_sector',
            '2g_trx',
            '3g_rnc',
            '3g_f1_f2',
            '3g_6sector',
            '3g_f3',
            '3g_bts_hotel',
            '3g_sector',
            '4g_enodeb',
            '4g_sector',
            'input_date'        
        ];
    }

    public function collection()
    {
        $get = collect(Api::dataResumeNe());
        return $get;
    }
}
