<?php

namespace App\Export;
use App\Api;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class BtsOnAirExportMon implements FromCollection, WithHeadings
{

    public function headings(): array
    {
        return [
            'regional',
            'bsc',
            'bts',
            'mc',
            'nodeb',
            'enodeb'
        ];
    }

    public function collection()
    {
        $get = collect(Api::dataBtsOnAirMonthly());
        return $get;
    }
}
