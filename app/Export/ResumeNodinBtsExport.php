<?php

namespace App\Export;
use App\Api;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ResumeNodinBtsExport implements FromCollection, WithHeadings
{

    public function headings(): array
    {
        $headings = [];
        array_push($headings, 'RADIO NETWORK CAPACITY BUILT (UNIT)');
        for ($i=1; $i < 7; $i++) {
            $date = date('F Y', strtotime('-'.$i.' month'));
            array_push($headings, $date);
        }
        return $headings;
    }

    public function collection()
    {
        $get = collect(Api::dataResumeNodinBts());

        $data[0] = [];
        $data[1] = [];
        $data[2] = [];
        $data[3] = [];
        $data[4] = [];
        $data[5] = [];
        $data[6] = [];
        $data[7] = [];
        $data[8] = [];
        $data[9] = [];
        $data[10] = [];
        $data[11] = [];
        $data[12] = [];
        $data[13] = [];
        $data[14] = [];
        $data[15] = [];
        $data[16] = [];
        $data[17] = [];
        array_push($data[0], 'BTS 2G - On Air This Year');
        array_push($data[1], 'BTS 2G - Dismantled');
        array_push($data[2], 'BTS 2G - Total On Air');
        array_push($data[3], 'BTS 3G - On This Year');
        array_push($data[4], 'BTS 3G - Dismantled');
        array_push($data[5], 'BTS 3G - Total On Air');
        array_push($data[6], 'BTS 4G - On This Year');
        array_push($data[7], 'BTS 4G - Dismantled');
        array_push($data[8], 'BTS 4G - Total On Air');
        array_push($data[9], 'TRX - On This Year');
        array_push($data[10], 'TRX - Dismantled');
        array_push($data[11], 'TRX - Total On Air');
        array_push($data[12], 'Jumlah BTS (2G, 3G & 4G)');
        array_push($data[13], 'Jumlah Site 2G Only');
        array_push($data[14], 'Jumlah Site 3G Only');
        array_push($data[15], 'Jumlah Site 4G Only');
        array_push($data[16], 'Jumlah Site 2G 3G & 4G');
        array_push($data[17], 'Total Jumlah Site');
        foreach ($get as $d) {
            foreach ($d as $key => $value) {
                if ($key == 'onair_2g') {
                    array_push($data[0], $value);
                }elseif ($key == 'offair_2g') {
                    array_push($data[1], $value);
                }elseif ($key == 'total_2g') {
                    array_push($data[2], $value);
                }elseif ($key == 'onair_3g') {
                    array_push($data[3], $value);
                }elseif ($key == 'offair_3g') {
                    array_push($data[4], $value);
                }elseif ($key == 'total_3g') {
                    array_push($data[5], $value);
                }elseif ($key == 'onair_4g') {
                    array_push($data[6], $value);
                }elseif ($key == 'offair_4g') {
                    array_push($data[7], $value);
                }elseif ($key == 'total_4g') {
                    array_push($data[8], $value);
                }elseif ($key == 'onair_trx') {
                    array_push($data[9], $value);
                }elseif ($key == 'offair_trx') {
                    array_push($data[10], $value);
                }elseif ($key == 'total_trx') {
                    array_push($data[11], $value);
                }elseif ($key == 'total_all') {
                    array_push($data[12], $value);
                }elseif ($key == 'only_2g') {
                    array_push($data[13], $value);
                }elseif ($key == 'only_3g') {
                    array_push($data[14], $value);
                }elseif ($key == 'only_4g') {
                    array_push($data[15], $value);
                }elseif ($key == 'collocation') {
                    array_push($data[16], $value);
                }elseif ($key == 'total_all_site') {
                    array_push($data[17], $value);
                }
            }
        }
        return collect($data);
    }
}
