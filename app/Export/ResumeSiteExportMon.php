<?php

namespace App\Export;
use App\Api;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ResumeSiteExportMon implements FromCollection, WithHeadings
{
    public function headings(): array
    {
        return [
            'regional',
            '2g3g',
            '2g3g4g',
            '2g4g',
            '2g_only',
            '3g4g',
            '3g_only',
            '4g_only',
            'total'
        ];  
    }

    public function collection()
    {
        $get = collect(Api::dataResumeSiteMonthly());
        return $get;
    }
}
