<?php header('Access-Control-Allow-Origin: *'); ?> 
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>NEISA | DASHBOARD</title>
    <!-- Tell the browser to be responsive to screen width -->
    
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <!-- {{-- <link rel="stylesheet" href="{{url('')}}/plugins/font-awesome/css/font-awesome.min.css"> --}} -->
    <link rel="stylesheet" href="https://unpkg.com/material-components-web@1.0.1/dist/material-components-web.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{url('')}}/dist/css/adminlte.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{url('')}}/plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="{{url('')}}/plugins/morris/morris.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <!-- DataTables -->
    <link rel="stylesheet" href="{{url('')}}/plugins/datatables/dataTables.bootstrap4.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{url('')}}/dist/css/adminlte.min.css">
    <link rel="stylesheet" href="{{url('')}}/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    
    <script> var __basePath = ''; </script>
    <style> html, body { margin: 0; padding: 0; height: 100%; } </style>
    <script src="https://unpkg.com/ag-grid-enterprise@20.2.0/dist/ag-grid-enterprise.min.js"></script>  
    
    <!-- *** START TAMBAH IMPORT FONTAWESOME IYON *** -->
    <link rel="stylesheet" type="text/css" href="{{url('')}}/plugins/fontawesome-free-5.6.3-web/css/fontawesome.css">
    <link rel="stylesheet" type="text/css" href="{{url('')}}/plugins/fontawesome-free-5.6.3-web/css/solid.css">
    <link rel="stylesheet" type="text/css" href="{{url('')}}/plugins/fontawesome-free-5.6.3-web/css/brands.css">
    <!-- *** END TAMBAH IMPORT FONTAWESOME IYON *** -->
    
    <style>
        /* ** START TAMBAH CSS IYON** */
        .dropClass {
            /* display: none; */
            visibility: hidden;
            opacity: 0;
            transition:
            all .5s ease;
            background-color: transparent;
            min-width: 160px;
            overflow: auto;
            z-index: 1;
            height: 0;
            /* margin-bottom: -5%; */
        }
        .dropClass a {
            color: black;
            padding: 12px 16px;
            /* text-decoration: none; */
            display: block;
        }
        .dropClass a:hover {background-color: rgba(255,255,255,.1);}
        .show {
            /* display: block; */
            visibility: visible;
            opacity: 1;
            height: auto;
            padding: 0.5rem 1rem;
            background-color: rgba(255,255,255,.3);
            margin-bottom: 1.5%;
            border-radius: 3%;
        }
        .putar {
            transform: rotate(90deg);
            transition: all .5s ease;
        }
        .brand-image {
            line-height: .8;
            max-height: 53px;
            width: auto;
            margin-left: 0.7rem;
            margin-right: .5rem;
            margin-top: -3px;
            float: none;
            opacity: .9;
        }
        .backgroundImg { 
            width: auto;
            height: 100%;
            opacity: 1;
            position: absolute;
        }
        .backgroundImg2 {
            position: fixed;
            width: 100%;
            max-height: 56px;
            margin-left: -2%;
            opacity: 1;
        }
        .nav-item:hover {
            background-color: rgba(255,255,255,.3);
            border-radius: 5%;
            transition: all .2s ease;
        }
        .active {
            background-color: rgba(243, 255, 226, .8) !important;
            color: #343a40 !important;
            font-weight: 600;
        }
        .berisik {
            min-height:500px !important
        }
        .tesDiv {
            z-index: -1;
            opacity: .4;
            background: url(./dist/img/tesblek.png) center center
        }
        .tesDiv .bekgron{
            z-index: 1;
            opacity: 1
        }
        /* ** END TAMBAH IYON** */
        /* ** START UI BARU** */
        #bungkus {
            /* background: url(./dist/img/darkwall6.jpg) center center; */
            background-image:linear-gradient(to right, #0F2027,#203A43,#2C5364, #203A43, #0F2027);
        }
        .garisijo {
            background-color: rgba(150, 178, 138, 1);
            height: 3px;
            width: 100%;
            position: absolute;
            bottom: 0;
            left: 0;
            /* margin-left: -6%; */
        }
        .teksboks {
            width: 85%;
            position: absolute;
            bottom: 0;
            left: 0;
        }
        .teksne{
            /* bottom: 0;
            right: 3%; */
            margin:auto;
            position: absolute;
        }
        .boksHead {
            font-size: 30px;
            /* box-shadow: 0 3px 1px 0 rgba(0, 0, 0, 0.2), 0 1px 0px 0 rgba(0, 0, 0, 0.19); */
            padding: 10px;
            font-weight: 500;
            border-radius: 0;
            background-color: rgba(255, 255, 255, .2);
            color: #96b28a;
            font-weight: bold;
            box-shadow: none;
            margin-bottom:0 !important;
        }
        .boksBody {
            height: 90px;
            /* background-color: #343a40; */
            border-radius: 0;
            background-color: rgba(255, 255, 255, .2);
            /* background-image: linear-gradient(to top, rgba(0,255,255,.2), rgba(150, 178, 138, .5)); */
            /* background-color: rgba(0, 0, 0, .5); */
            box-shadow: none;
        }
        .inner {
            padding:0 !important;
        }
        .card.chartcard {
            background-color:transparent;
            border: 0;
            border-radius: 0;
            box-shadow: none;
        }
        table {
            font-size: 14px;
            background-color: rgba(1, 14, 23,.9)!important;
            color:lightgrey;
        }
        /* tr:hover {
            background-color: rgba(255, 255, 255, .2)
        } */
        tr > td:hover {
            background-color: rgba(1, 14, 23,.9)!important;
            color:lightgrey;
        }
        
        .table td, .table th {
            border: 1px solid #ddd;
        }
        
        thead {
            background-color: rgba(1, 14, 23,.9)!important;
            color:lightgrey;
        }
        .kepanjangan {
            font-size: 10px;
        }
        .kepanjangantot {
            font-size: 12px;
        }
        /* ** END UI BARU** */
        
        .page-link {
            color: #fff;
            background-color: #262c2e;
        }
        
        .social-part .fa{
            padding-right:20px;
        }
        ul li a{
            margin-right: 20px;
        }
        
        .bg-light {
            color:#fff;
            background-image:linear-gradient(to right, #1c1f20, #2d3436, #2d3436, #2d3436);
        } 
        
        .bg-light, .bg-light a {
            color: #fff!important;
        }
        
        .dropdown-menu {
            background-color: #262c2e;
            color: #0F2027;
        }
        
        .active1 {
            background-color: #6c7173; !important;
            color: #343a40 !important;
            font-weight: 600;
        }
        
        .item:hover {
            background-color: #0F2027;
        }
        
        .invisible {
            visibility: hidden;
        }

        .dropdown-item:focus, .dropdown-item:hover {
            background-color: #6c7173;
        }
        
        .form-control2 {
            font-size: 16px !important;
            background-color:transparent !important;
            border:none !important;
            color: #fff !important;
            border-radius: 0 !important;
            padding: 0 !important;
            padding-left: 4px !important;
            padding-top: 0px !important;
            padding-bottom: 0px !important;
            background: #000;
        }

    </style>
    
</head>
<body class="hold-transition sidebar-mini " style="background: #f4f6f9; color: white;">
    <div class="wrapper  ">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand bg-dark" style="margin-left: 250px; position:fixed; width:100%;">
            <img src="{{url('')}}/dist/img/wall5.jpg" class="backgroundImg2" style="position: fixed;width: 100%;">
            <!-- {{-- <img src="./dist/img/wall5.jpg" class="backgroundImg2" style="position: fixed;
            width: 100%;"> --}} -->
            <!-- Left navbar links -->
            <ul class="navbar-nav" style="z-index: 999;">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars nav-button" style="color:white;"></i></a>
                </li>
                <li class=" navbar-brand" style="color:white; margin-left: 10%;">NEISA | DASHBOARD</li>
                <li class="nav-item">
                    <a class="nav-link btn btn-lg" href="http://10.54.36.49/landingPage/" onclick="sessionStorage.clear();" style="
                    color: #343a40 !important;
                    background-color: #fff;
                    position: fixed;
                    font-size: 10px;
                    right: 1%;
                    height: auto;
                    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
                    text-transform: uppercase;
                    font-family: Roboto;
                    padding: 1%;"><i class="fa fa-sign-out-alt"></i> Log Out</a>
                </li>
            </ul>
        </nav>
        
        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4 berisik">
            <img src="{{url('')}}/dist/img/wall3.jpg" class="backgroundImg">
            <!-- {{-- <img src="./dist/img/wall3.jpg" class="backgroundImg"> --}} -->
            <!-- Brand Logo -->
            <a href="#" class="brand-link">
                <img src="{{url('')}}/dist/img/tsel-white.png"
                style="opacity: .8; float:none; widht:200px; line-height:.8; max-height:53px;margin-left:0.7rem;margin-right:.5rem;margin-top:-3px">
            </a>
            
            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <li class="nav-item">
                            <a href="http://10.54.36.49/dashboard-bts-on-air/public/" class="nav-link" style="color: #fff;padding: 0.5rem 1rem !important;">
                                <i class="nav-icon fa fa-home"></i>
                                <p style="margin-left: 3px;">Dashboard</p>
                            </a>
                        </li>
                        <li  class="nav-item">
                            <a href="http://10.54.36.49/dashboard-license" class="nav-link aa " style="color: #fff;padding: 0.5rem 1rem !important;">
                                <i class="nav-icon fa fa-newspaper"></i>
                                <p style="margin-left: 3px;"> License</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="http://10.54.36.49/btsonair" class="nav-link " style="color: #fff;padding: 0.5rem 1rem !important;">
                                <i class="nav-icon fa fa-broadcast-tower"></i>
                                <p style="margin-left: 3px;">BTS Status</p>
                            </a>
                        </li> 
                        <li class="nav-item" style="cursor: pointer;">
                            <a onclick="dropDead()" class="nav-link aa dropbtn" style="color: #fff;padding: 0.5rem 1rem !important;">
                                <i class="nav-icon fa fa-angle-right" id="dropIcon"></i>
                                <p style="margin-left: 3px;">Create</p>
                            </a>
                        </li>
                        <div class="dropClass" id="dropId">
                            <li class="nav-item">
                                <a href="http://10.54.36.49/apk-nodin/index.php/NodinController" class="nav-link aa" style="color: #fff;padding: 0.5rem 1rem !important;">
                                    <i class="nav-icon fa fa-list-alt"></i>
                                    <p style="margin-left: 3px;">Create Integrasi</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="http://10.54.36.49/apk-nodin-stylo/index.php/NodinController" class="nav-link aa" style="color: #fff;padding: 0.5rem 1rem !important;">
                                    <i class="nav-icon fa fa-list-alt"></i>
                                    <p style="margin-left: 3px;">Create Rehoming</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="http://10.54.36.49/apk-nodin-dismantle/index.php/NodinController" class="nav-link aa " style="color: #fff;padding: 0.5rem 1rem !important;">
                                    <i class="nav-icon fa fa-list-alt"></i>
                                    <p style="margin-left: 3px;">Create Dismantle</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="http://10.54.36.49/apk-nodin-relocation/index.php/NodinController" class="nav-link aa " style="color: #fff;padding: 0.5rem 1rem !important;">
                                    <i class="nav-icon fa fa-list-alt"></i>
                                    <p style="margin-left: 3px;">Create Relocation</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="http://10.54.36.49/apk-nodin-swap/index.php/NodinController" class="nav-link aa" style="color: #fff;padding: 0.5rem 1rem !important;">
                                    <i class="nav-icon fa fa-list-alt"></i>
                                    <p style="margin-left: 3px;">Create Swap</p>
                                </a>
                            </li>
                        </div>
                        <li class="nav-item">
                            <a href="http://10.54.36.49/change-front-2/public/" class="nav-link aa " style="color: #fff;padding: 0.5rem 1rem !important;">
                                <i class="nav-icon fa fa-project-diagram"></i>
                                <p style="margin-left: 3px;">Process Tracking</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="http://10.54.36.49/tableList" class="nav-link aa" style="color: #fff;padding: 0.5rem 1rem !important;">
                                <i class="nav-icon fa fa-table"></i>
                                <p style="margin-left: 3px;">Nodin & MoM Report</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="http://10.54.36.49/apk-report/index.php/ReportController" class="nav-link aa " style="color: #fff;padding: 0.5rem 1rem !important;">
                                <i class="nav-icon fa fa-book"></i>
                                <p style="margin-left: 3px;">Report Remedy</p>
                            </a>
                        </li>
                        <li class="nav-item"><li class="nav-item">
                            <a href="http://10.54.36.49/approval_site/public/" class="nav-link aa active" style="color: #fff;padding: 0.5rem 1rem !important;">
                                <i class="nav-icon fa fa-list-alt"></i>
                                <p style="margin-left: 3px;">Approval Site</p>
                            </a>
                        </li>
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>
        
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper" id="bungkus" style="margin-top:55px;">
                    <?php if ($rule_user == "Project"){
                    ?>
                    <nav class="navbar navbar-expand-sm navbar-light bg-light">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                            <li class="nav-item"> 
                                <a class="dropdown-item" href="{{url('/')}}">Baseline</a>
                            </li>
                            <li class="nav-item">
                                <a class="dropdown-item active1" href="{{url('baseline3g')}}">Insert Baseline</a>
                            </li>
                        </ul>
                        </div>
                    </nav> 
                    <?php
                    }?>
                    <section class="content" >
                        
                        <div class="container-fluid" >
                            <section class="col-lg-12" id="seksion1">
                                
                                <div class="box box-primary">

                                    <b><h5 style="text-align:center; padding-top: 15px;">Approval Site</h5></b>
                                    <?php if ($rule_user == "Project"){
                                    ?>
                                    <button onclick="onInsertRowAt2()" class="btn btn-primary btn-sm">Insert Row</button>
                                    <button onclick="onRemoveSelected()" class="btn btn-primary btn-sm">Remove Selected</button>
                                    <button onclick="onEditingSelected()" class="btn btn-primary btn-sm">Insert Selected</button>
                                    <?php
                                    }else{ 
                                    ?>
                                    <button onclick="onEditingSelected()" class="btn btn-primary btn-sm">Insert Selected</button>
                                    <?php
                                    }?>

                                    <!-- @if (session('success'))
                                    <div style="margin-top:10px" class="alert alert-success alert-dismissible">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            Import Data Success
                                    </div> 
                                    @endif 
                                    @if (session('error')) 
                                    <div style="margin-top:10px" class="alert alert-danger alert-dismissible">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            Import Data Failed
                                    </div>
                                    @endif 

                                    <div class="box-body">

                                        <div class="row"> 
                                            <div class="col-md-12">
                                                
                                                {{-- <div class="row"> 
                                                <form class="form-inline float-right">
                                                    <div class="form-group mx-sm-3 mb-2"> 
                                                        <input class="form-control" id="search_data" placeholder="Search Cell Name">
                                                    </div>  
                                                    <button id="btn_search" onclick="onBtnSearch()" type="button" class="btn btn-light mb-2">Search</button>
                                                </form>
                                            </div>   --}}
                                            
                                                <!-- <div class="row " style="margin-top: 5px; margin-bottom: 10px; margin-left:2px;">
                                                    <a href="{{url('baseline/exportCSVbtsonair4g')}}" class="btn btn-primary btn-sm" role="button">Export To Excel </a>                                                   
                                                </div> -->

                                                <div style="height: 670px;" id="myGrid" class="ag-theme-balham-dark"> </div> 
                                                <!-- <?php if ($rule_user == "fixed"){
                                                ?>
                                                <div style="margin-top:10px" class="row">
                                                    <div class="col-5">
                                                                <form action="{{ url('/import/btsonair4gimport') }}" method="post" enctype="multipart/form-data">
                                                                    @csrf
                                                                    <div>
                                                                          
                                                                        <div style="" class="btn-group" role="group"> 
                                                          
                                                                            <button class="btn btn-primary btn-sm">Submit</button>
                
                                                                            <input style="background:#000" type="file" class="form-control2" name="file_import">
                                                                            <p class="text-danger">{{ $errors->first('file') }}</p>
                                                                        </div>
                 
                                                                    </div> 
                                                                </form>
 
                                                        </div>
                                                        <div class="col-3"></div>
                                                        <div class="col-4">
                                                                <a style="right:0; margin-right:6px; position: absolute;" href="{{ url('/template/sysinfo4g') }}" class="btn btn-primary btn-sm" role="button">Download Template</a>  
                                                        </div> 
                                                    </div>
                                                    <?php
                                                    }?> -->

                                                    <div style="margin-top: 5px"> 
                                                        <span class="label invisible">Last Page Found:</span> 
                                                        <span class="value invisible" id="lbLastPageFound">-</span>
                                                        <span class="label invisible">Page Size:</span>
                                                        <span class="value invisible" id="lbPageSize">-</span>
                                                        <span class="label invisible">Total Pages:</span>
                                                        <span class="value invisible" id="lbTotalPages">-</span>
                                                        <span class="label invisible">Current Page:</span>
                                                        <span class="value invisible" id="lbCurrentPage">-</span>
                                                    </div> 
                                                    
                                                    {{-- <div style="height: calc(100% - 60px);" id="myGrid" class="ag-theme-balham-dark"/> --}}                   


<?php
if($rule_user == "Asset"){
?>
<script>  
        var id = 0;       
        var limit = 50;     
        var offset = 0;
        var rowData = []; 

        var columnDefs = [  
        {headerName: 'Regional', field: 'REGIONAL', width: 140,filterParams: {newRowsAction: 'keep'}},
        {headerName: 'Area', field: 'AREA', width: 140,filterParams: {newRowsAction: 'keep'}},
        {headerName: 'Vendor', field: 'VENDOR', width: 140,filterParams: {newRowsAction: 'keep'}},
        {headerName: 'EnodeB Name', field: 'ENODEB_NAME', width: 140,filterParams: {newRowsAction: 'keep'}},                                           
        {headerName: 'Cell Name', field: 'CELL_NAME', width: 140,filterParams: {newRowsAction: 'keep'}},                                           
        {headerName: 'Ne ID', field: 'NEID', width: 140,filterParams: {newRowsAction: 'keep'}},                                           
        {headerName: 'Site ID', field: 'SITEID', width: 140,filterParams: {newRowsAction: 'keep'}},                                           
        {headerName: 'TAC', field: 'TAC', width: 140,filterParams: {newRowsAction: 'keep'}},                                           
        {headerName: 'EnodeB ID', field: 'ENODEBID', width: 140,filterParams: {newRowsAction: 'keep'}},                                           
        {headerName: 'CI', field: 'CI', width: 140,filterParams: {newRowsAction: 'keep'}},
        {headerName: 'EARNFCN', field: 'EARNFCN', width: 140,filterParams: {newRowsAction: 'keep'}},
        {headerName: 'PID', field: 'PID', width: 140,filterParams: {newRowsAction: 'keep'}},
        {headerName: 'Frequency', field: 'FREQUENCY', width: 140,filterParams: {newRowsAction: 'keep'}},
        {headerName: 'Bandtype', field: 'BANDTYPE', width: 140,filterParams: {newRowsAction: 'keep'}},
        {headerName: 'Bandwith', field: 'BANDWITH', width: 140,filterParams: {newRowsAction: 'keep'}},
        {headerName: 'Jumlah EnodeB', field: 'JMLH_ENODEB', width: 140,filterParams: {newRowsAction: 'keep'}},
        {headerName: 'Jumlah Cell', field: 'JMLH_CELL', width: 140,filterParams: {newRowsAction: 'keep'}},
        {headerName: 'Metro E', field: 'METRO_E', width: 140,filterParams: {newRowsAction: 'keep'}},
        {headerName: 'Owner Link', field: 'OWNER_LINK', width: 140,filterParams: {newRowsAction: 'keep'}},
        {headerName: 'Tipe Link', field: 'TIPE_LINK', width: 140,filterParams: {newRowsAction: 'keep'}},
        {headerName: 'Far End Link', field: 'FAR_END_LINK', width: 140,filterParams: {newRowsAction: 'keep'}},
        {headerName: 'Total Bandwith', field: 'TOTAL_BANDWITH', width: 140,filterParams: {newRowsAction: 'keep'}},
        {headerName: 'Tanggal OnAir Lease Line', field: 'TANGGAL_ONAIR_LEASE_LINE', width: 140,filterParams: {newRowsAction: 'keep'}},
        {headerName: 'Site Simpul', field: 'SITE_SIMPUL', width: 140,filterParams: {newRowsAction: 'keep'}},
        {headerName: 'Jumlah Site Under Simpul', field: 'JUMLAH_SITE_UNDER_SIMPUL', width: 140,filterParams: {newRowsAction: 'keep'}},
        {headerName: 'Status Lokasi', field: 'STATUS_LOKASI', width: 140,filterParams: {newRowsAction: 'keep'}},
        {headerName: 'Cluster Sales', field: 'CLUSTER_SALES', width: 140,filterParams: {newRowsAction: 'keep'}},
        {headerName: 'Type BTS', field: 'TYPE_BTS', width: 140,filterParams: {newRowsAction: 'keep'}},
        {headerName: 'Status', field: 'STATUS', width: 140,filterParams: {newRowsAction: 'keep'}},
        {headerName: 'New Existing', field: 'NEW_EXISTING', width: 140,filterParams: {newRowsAction: 'keep'}},
        {headerName: 'OnAir', field: 'ONAIR', width: 140,filterParams: {newRowsAction: 'keep'}},
        {headerName: 'Date OnAir', field: 'DATE_ONAIR', width: 140,filterParams: {newRowsAction: 'keep'}},

        {headerName: 'KPI PASS', field: 'KPI_PASS', width: 140,filterParams: {newRowsAction: 'keep'}},
        {headerName: 'Date KPI PASS', field: 'DATE_KPI_PASS', width: 140,filterParams: {newRowsAction: 'keep'}},
        {headerName: 'Remark', field: 'REMARK', width: 140,filterParams: {newRowsAction: 'keep'}},
        {headerName: 'Departement', field: 'DEPARTEMENT', width: 140,filterParams: {newRowsAction: 'keep'}},
        {headerName: 'Technical Area', field: 'TECHNICAL_AREA', width: 140,filterParams: {newRowsAction: 'keep'}},
        {headerName: 'Longitude', field: 'LONGITUDE', width: 140,filterParams: {newRowsAction: 'keep'}},
        {headerName: 'Latitude', field: 'LATITUDE', width: 140,filterParams: {newRowsAction: 'keep'}},
        {headerName: 'Alamat', field: 'ALAMAT', width: 140,filterParams: {newRowsAction: 'keep'}},
        {headerName: 'Kelurahan', field: 'KELURAHAN', width: 140,filterParams: {newRowsAction: 'keep'}},
        {headerName: 'Kecamatan', field: 'KECAMATAN', width: 140,filterParams: {newRowsAction: 'keep'}},
        {headerName: 'Kabupaten', field: 'KABUPATEN', width: 140,filterParams: {newRowsAction: 'keep'}},
        {headerName: 'Provinsi', field: 'PROVINSI', width: 140,filterParams: {newRowsAction: 'keep'}},
        {headerName: 'Tower Provider', field: 'TOWER_PROVIDER', width: 140,filterParams: {newRowsAction: 'keep'}},
        {headerName: 'Nama Tower Provider', field: 'NAMA_TOWER_PROVIDER', width: 140,filterParams: {newRowsAction: 'keep'}},
        {headerName: 'Status PLN', field: 'STATUS_PLN', width: 140,filterParams: {newRowsAction: 'keep'}},
        {headerName: 'Vendor FMC', field: 'VENDOR_FMC', width: 140,filterParams: {newRowsAction: 'keep'}},

        {headerName: 'Status Asset', field: 'STATUS_ASSET', type: 'EditableColumn', width: 140, suppressSizeToFit: true,filterParams: {newRowsAction: 'keep'},
        cellEditorSelector:function (params){  
            if (params.data.STATUS_ASSET === 'Approval' || params.data.STATUS_ASSET === 'Rejected' || params.data.STATUS_ASSET === ""
                ) return {
                component: 'agRichSelectCellEditor', 
            params: {values: ['Approval', 'Rejected']}
        };     
        return null;                     }                                                 
    },
    {headerName: 'Status RAO', field: 'STATUS_RAO', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Status Project', field: 'STATUS_PROJECT', width: 140,filterParams: {newRowsAction: 'keep'}},



    ];
    
    var gridOptions = {
        defaultColDef: {
            editable: false,
            resizable: true,
            filter: true,
        },
        columnTypes: { 
            EditableColumn: {editable: true},
        },
        
        debug: true,
        rowSelection: 'multiple',
        paginationPageSize: limit,
        columnDefs: columnDefs,      
        // sideBar: true,
        paginationAutoPageSize:true,
        pagination: true,
        // suppressPaginationPanel: true,
        suppressScrollOnNewData: true, 
        // onPaginationChanged: onPaginationChanged,
        onSelectionChanged: onSelectionChanged,
        onCellEditingStopped: onCellEditingStopped,
    };
    
    function setText(selector, text) {
        document.querySelector(selector).innerHTML = text;
    }   
    
    function onPaginationChanged() {
        console.log('onPaginationPageLoaded');
        
        // Workaround for bug in events order
        if (gridOptions.api) {
            setText('#lbLastPageFound', gridOptions.api.paginationIsLastPageFound());
            setText('#lbPageSize', gridOptions.api.paginationGetPageSize());
            // we +1 to current page, as pages are zero based
            setText('#lbCurrentPage', gridOptions.api.paginationGetCurrentPage() + 1);
            setText('#lbTotalPages', gridOptions.api.paginationGetTotalPages());
            
            setLastButtonDisabled(!gridOptions.api.paginationIsLastPageFound());
        }  
    } 
    
    function setLastButtonDisabled(disabled) {
        document.querySelector('#btLast').disabled = disabled;
    }
    
    function setRowData(rowData) {
        allOfTheData = rowData;
        createNewDatasource();
    }
    
    function onSelectionChanged() {
        var selectedRows = gridOptions.api.getSelectedRows();
        
        // console.log(selectedRows);
        
        var selectedRowsString = []; 
        selectedRows.forEach( function(selectedRow, index) {
            if (index!==0) {
                selectedRowsString += ', ';
            }
            selectedRowsString.push(selectedRow);
        });
        return JSON.stringify({ data : selectedRowsString });
    }
    
    // function onCellEditingStopped() { 
    //     var selectedRows = gridOptions.api.getSelectedRows();
        
    //     console.log(selectedRows); 
        
    //     var selectedRowsString = [];
    //     selectedRows.forEach( function(selectedRow, index) {
    //         if (index!==0) {
    //             selectedRowsString += ', ';
    //         }
    //         selectedRowsString.push(selectedRow);
    //     });
    //     datass = JSON.stringify({ data : selectedRowsString });
        
    //     console.log(datass);
          
    //     var data = [];
    //     var httpRequest = new XMLHttpRequest();
    //     var api = 'http://10.54.36.49/api-approval/public/api/onair_bts_4g_update';
    //     data = datass;
    //     httpRequest.onload = function (data) {
    //         console.log(data); 
    //     };  
        
    //     httpRequest.open('PUT',api,true);
    //     httpRequest.setRequestHeader("Content-type", "application/json; charset=utf-8");
    //     httpRequest.send(data);
    // } 
    function onEditingSelected() { 
        var selectedRows = gridOptions.api.getSelectedRows();
        
        console.log(selectedRows); 
        
        var selectedRowsString = [];
        selectedRows.forEach( function(selectedRow, index) {
            if (index!==0) {
                selectedRowsString += ', ';
            }
            selectedRowsString.push(selectedRow);
        });
        datass = JSON.stringify({ data : selectedRowsString });
        
        console.log(datass);
        
        var data = [];
        var httpRequest = new XMLHttpRequest();
        var api = 'http://10.54.36.49/api-approval/public/api/onair_bts_4g_update';
        data = datass;
        httpRequest.onload = function (data) {
            console.log(data); 
        };  
        
        httpRequest.open('PUT',api,true);
        httpRequest.setRequestHeader("Content-type", "application/json; charset=utf-8");
        httpRequest.send(data);

        alert('Insert Data Success!');
        location.reload();
    } 
    
    function onCellEditingStopped() { 
        var selectedRows = gridOptions.api.getSelectedRows();
        
        console.log(selectedRows); 
        
        // var selectedRowsString = [];
        // selectedRows.forEach( function(selectedRow, index) {
        //     if (index!==0) {
        //         selectedRowsString += ', ';
        //     }
        //     selectedRowsString.push(selectedRow);
        // });
        // datass = JSON.stringify({ data : selectedRowsString });
        
        // console.log(datass);
        
        var data = [];
        var httpRequest = new XMLHttpRequest();
        var api = 'http://10.54.36.49/api-approval/public/api/onair_bts_4g_update';
        data = datass;
        httpRequest.onload = function (data) {
            console.log(data); 
        };  
        
        httpRequest.open('PUT',api,true);
        httpRequest.setRequestHeader("Content-type", "application/json; charset=utf-8");
        httpRequest.send(data);
    } 
    
    document.addEventListener('keyup',function(event){
        if (event.keyCode == 13) { 
            var data = [];
            var httpRequest = new XMLHttpRequest();
            var api = 'http://10.54.36.49/api-approval/public/api/onair_bts_4g_update';
            data = onSelectionChanged();
            httpRequest.onload = function (data) {
                console.log(data);
            };  
            
            console.log(data);
            
            httpRequest.open('PUT',api,true);
            httpRequest.setRequestHeader("Content-type", "application/json; charset=utf-8");
            httpRequest.send(data);
        } 
    }); 
    
    function onBtNext() {
        id = 0; 
        offset = limit;
        limit += 50;  
              
        console.log(limit);
        
        // setup the grid after the page has finished loading
        document.getElementById("btnNext").addEventListener('click', function() {
            document.querySelector('#myGrid');
            var httpRequest = new XMLHttpRequest();

            search = document.getElementById("search_data").value; 
            var regional_user = <?php echo json_encode($regional_user); ?>;
            if(search) { 
            // var api = encodeURI('http://10.54.36.49/api-approval/public/api/onair_bts_4g');
            var api = encodeURI('http://10.54.36.49/api-approval/public/api/onair_bts_2g?search=' + search + '&limit=' + limit + '&offset=' + offset);
            } else { 
            var api = encodeURI('http://10.54.36.49/api-approval/public/api/onair_bts_2g?search=' + search + '&limit=' + limit + '&offset=' + offset);
            }

            httpRequest.open('GET', api); 
            httpRequest.send(); 
            httpRequest.onreadystatechange = function() {
                if (httpRequest.readyState === 4 && httpRequest.status === 200) {
                    var httpResponse = JSON.parse(httpRequest.responseText);
                    gridOptions.api.setRowData(httpResponse.data);
                }
            };
        });
        
        gridOptions.api.paginationGoToNextPage();
    }
    
    function onBtPrevious() { 
        id = 0; 
        offset -= limit;
        limit -= 50;
        
        // setup the grid after the page has finished loading
        document.getElementById("btnPrevious").addEventListener('click', function() {
            document.querySelector('#myGrid');
            var httpRequest = new XMLHttpRequest();

            search = document.getElementById("search_data").value; 
            var regional_user = <?php echo json_encode($regional_user); ?>;
            if(search) { 
            var api = encodeURI('http://10.54.36.49/api-approval/public/api/onair_bts_2g?search=' + search + '&limit=' + limit + '&offset=' + offset);
            } else {
            var api = encodeURI('http://10.54.36.49/api-approval/public/api/onair_bts_2g?search=' + search + '&limit=' + limit + '&offset=' + offset);
            }

            httpRequest.open('GET', api);
            httpRequest.send();
            httpRequest.onreadystatechange = function() {
                if (httpRequest.readyState === 4 && httpRequest.status === 200) {
                    var httpResponse = JSON.parse(httpRequest.responseText);
                    gridOptions.api.setRowData(httpResponse.data);
                }
            };
        });
        
        gridOptions.api.paginationGoToPreviousPage();
    }  
    
    // function onBtnSearch() { 
    //     id = 0; 
    //     offset = limit; 
    //     limit += 50;   
    //     search = document.getElementById("search_data").value;
    //     console.log(search);   
        
    //     // setup the grid after the page has finished loading
    //     document.getElementById("btn_search").addEventListener('click', function() {  
    //         document.querySelector('#myGrid');   
    //         var httpRequest = new XMLHttpRequest();
    //         var regional_user = <?php echo json_encode($regional_user); ?>;
    //         var api = encodeURI('http://10.54.36.49/api-btsonair/public/api/onair_bts_4g?regional=' + regional_user + '&search=' + search + '&limit=' + limit + '&offset=' + offset);
    //         httpRequest.open('GET', api);     
    //         httpRequest.send(); 
    //         httpRequest.onreadystatechange = function() {
    //             if (httpRequest.readyState === 4 && httpRequest.status === 200) {
    //                 var httpResponse = JSON.parse(httpRequest.responseText);
    //                 gridOptions.api.setRowData(httpResponse.data);
    //             }
    //         };     
    //     });   
    // }   
    
    // setup the grid after the page has finished loading
    document.addEventListener('DOMContentLoaded', function() { 
        // var search = document.getElementById("search_data").value;
        var gridDiv = document.querySelector('#myGrid');
        new agGrid.Grid(gridDiv, gridOptions); 
        // do http request to get our sample data - not using any framework to keep the example self contained.
        // you will probably use a framework like JQuery, Angular or something else to do your HTTP calls.
        var httpRequest = new XMLHttpRequest();  
        var regional_user = <?php echo json_encode($regional_user); ?>;
        // var api = encodeURI('http://10.54.36.49/api-approval/public/api/onair_bts_4g');
        var api = encodeURI('http://10.54.36.49/api-approval/public/api/onair_bts_2g');
        console.log(api);
        httpRequest.open('GET', api); 
        httpRequest.send(); 
        httpRequest.onreadystatechange = function() {
            if (httpRequest.readyState === 4 && httpRequest.status === 200) {
                var httpResponse = JSON.parse(httpRequest.responseText);
                gridOptions.api.setRowData(httpResponse.data);
            }
        }; 
    });
    
</script>    
<?php
} else if ($rule_user == "RAO"){
?>
<script>  
    var id = 0;       
    var limit = 50;     
    var offset = 0;
    var rowData = []; 

    var columnDefs = [  
    {headerName: 'Regional', field: 'REGIONAL', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Area', field: 'AREA', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Vendor', field: 'VENDOR', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'EnodeB Name', field: 'ENODEB_NAME', width: 140,filterParams: {newRowsAction: 'keep'}},                                           
    {headerName: 'Cell Name', field: 'CELL_NAME', width: 140,filterParams: {newRowsAction: 'keep'}},                                           
    {headerName: 'Ne ID', field: 'NEID', width: 140,filterParams: {newRowsAction: 'keep'}},                                           
    {headerName: 'Site ID', field: 'SITEID', width: 140,filterParams: {newRowsAction: 'keep'}},                                           
    {headerName: 'TAC', field: 'TAC', width: 140,filterParams: {newRowsAction: 'keep'}},                                           
    {headerName: 'EnodeB ID', field: 'ENODEBID', width: 140,filterParams: {newRowsAction: 'keep'}},                                           
    {headerName: 'CI', field: 'CI', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'EARNFCN', field: 'EARNFCN', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'PID', field: 'PID', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Frequency', field: 'FREQUENCY', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Bandtype', field: 'BANDTYPE', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Bandwith', field: 'BANDWITH', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Jumlah EnodeB', field: 'JMLH_ENODEB', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Jumlah Cell', field: 'JMLH_CELL', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Metro E', field: 'METRO_E', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Owner Link', field: 'OWNER_LINK', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Tipe Link', field: 'TIPE_LINK', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Far End Link', field: 'FAR_END_LINK', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Total Bandwith', field: 'TOTAL_BANDWITH', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Tanggal OnAir Lease Line', field: 'TANGGAL_ONAIR_LEASE_LINE', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Site Simpul', field: 'SITE_SIMPUL', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Jumlah Site Under Simpul', field: 'JUMLAH_SITE_UNDER_SIMPUL', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Status Lokasi', field: 'STATUS_LOKASI', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Cluster Sales', field: 'CLUSTER_SALES', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Type BTS', field: 'TYPE_BTS', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Status', field: 'STATUS', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'New Existing', field: 'NEW_EXISTING', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'OnAir', field: 'ONAIR', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Date OnAir', field: 'DATE_ONAIR', width: 140,filterParams: {newRowsAction: 'keep'}},

    {headerName: 'KPI PASS', field: 'KPI_PASS', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Date KPI PASS', field: 'DATE_KPI_PASS', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Remark', field: 'REMARK', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Departement', field: 'DEPARTEMENT', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Technical Area', field: 'TECHNICAL_AREA', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Longitude', field: 'LONGITUDE', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Latitude', field: 'LATITUDE', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Alamat', field: 'ALAMAT', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Kelurahan', field: 'KELURAHAN', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Kecamatan', field: 'KECAMATAN', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Kabupaten', field: 'KABUPATEN', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Provinsi', field: 'PROVINSI', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Tower Provider', field: 'TOWER_PROVIDER', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Nama Tower Provider', field: 'NAMA_TOWER_PROVIDER', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Status PLN', field: 'STATUS_PLN', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Vendor FMC', field: 'VENDOR_FMC', width: 140,filterParams: {newRowsAction: 'keep'}},

    {headerName: 'Status Asset', field: 'STATUS_ASSET', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Status RAO', field: 'STATUS_RAO', type: 'EditableColumn', width: 140, suppressSizeToFit: true,filterParams: {newRowsAction: 'keep'},
    cellEditorSelector:function (params){  
        if (params.data.STATUS_RAO === 'Approval' || params.data.STATUS_RAO === 'Rejected' || params.data.STATUS_RAO === ""
            ) return {
            component: 'agRichSelectCellEditor', 
        params: {values: ['Approval', 'Rejected']}
    };     
    return null;                     }                                                 
},
{headerName: 'Status Project', field: 'STATUS_PROJECT', width: 140,filterParams: {newRowsAction: 'keep'}},


];
    
    var gridOptions = {
        defaultColDef: {
            editable: false,
            resizable: true,
            filter: true,
        },
        columnTypes: { 
            EditableColumn: {editable: true},
        },
        
        debug: true,
        rowSelection: 'multiple',
        paginationPageSize: limit,
        columnDefs: columnDefs,      
        // sideBar: true,
        paginationAutoPageSize:true,
        pagination: true,
        // suppressPaginationPanel: true,
        suppressScrollOnNewData: true, 
        // onPaginationChanged: onPaginationChanged,
        onSelectionChanged: onSelectionChanged,
        onCellEditingStopped: onCellEditingStopped,
    };
    
    function setText(selector, text) {
        document.querySelector(selector).innerHTML = text;
    }   
    
    function onPaginationChanged() {
        console.log('onPaginationPageLoaded');
        
        // Workaround for bug in events order
        if (gridOptions.api) {
            setText('#lbLastPageFound', gridOptions.api.paginationIsLastPageFound());
            setText('#lbPageSize', gridOptions.api.paginationGetPageSize());
            // we +1 to current page, as pages are zero based
            setText('#lbCurrentPage', gridOptions.api.paginationGetCurrentPage() + 1);
            setText('#lbTotalPages', gridOptions.api.paginationGetTotalPages());
            
            setLastButtonDisabled(!gridOptions.api.paginationIsLastPageFound());
        }  
    } 
    
    function setLastButtonDisabled(disabled) {
        document.querySelector('#btLast').disabled = disabled;
    }
    
    function setRowData(rowData) {
        allOfTheData = rowData;
        createNewDatasource();
    }
    
    function onSelectionChanged() {
        var selectedRows = gridOptions.api.getSelectedRows();
        
        // console.log(selectedRows);
        
        var selectedRowsString = []; 
        selectedRows.forEach( function(selectedRow, index) {
            if (index!==0) {
                selectedRowsString += ', ';
            }
            selectedRowsString.push(selectedRow);
        });
        return JSON.stringify({ data : selectedRowsString });
    }
    
    // function onCellEditingStopped() { 
    //     var selectedRows = gridOptions.api.getSelectedRows();
        
    //     console.log(selectedRows); 
        
    //     var selectedRowsString = [];
    //     selectedRows.forEach( function(selectedRow, index) {
    //         if (index!==0) {
    //             selectedRowsString += ', ';
    //         }
    //         selectedRowsString.push(selectedRow);
    //     });
    //     datass = JSON.stringify({ data : selectedRowsString });
        
    //     console.log(datass);
          
    //     var data = [];
    //     var httpRequest = new XMLHttpRequest();
    //     var api = 'http://10.54.36.49/api-approval/public/api/onair_bts_4g_update';
    //     data = datass;
    //     httpRequest.onload = function (data) {
    //         console.log(data); 
    //     };  
        
    //     httpRequest.open('PUT',api,true);
    //     httpRequest.setRequestHeader("Content-type", "application/json; charset=utf-8");
    //     httpRequest.send(data);
    // }
    function onEditingSelected() { 
        var selectedRows = gridOptions.api.getSelectedRows();
        
        console.log(selectedRows); 
        
        var selectedRowsString = [];
        selectedRows.forEach( function(selectedRow, index) {
            if (index!==0) {
                selectedRowsString += ', ';
            }
            selectedRowsString.push(selectedRow);
        });
        datass = JSON.stringify({ data : selectedRowsString });
        
        console.log(datass);
        
        var data = [];
        var httpRequest = new XMLHttpRequest();
        var api = 'http://10.54.36.49/api-approval/public/api/onair_bts_4g_update';
        data = datass;
        httpRequest.onload = function (data) {
            console.log(data); 
        };   
        
        httpRequest.open('PUT',api,true);
        httpRequest.setRequestHeader("Content-type", "application/json; charset=utf-8");
        httpRequest.send(data);

        alert('Insert Data Success!');
        location.reload();

    } 
    
    function onCellEditingStopped() { 
        var selectedRows = gridOptions.api.getSelectedRows();
        
        console.log(selectedRows); 
        
        // var selectedRowsString = [];
        // selectedRows.forEach( function(selectedRow, index) {
        //     if (index!==0) {
        //         selectedRowsString += ', ';
        //     }
        //     selectedRowsString.push(selectedRow);
        // });
        // datass = JSON.stringify({ data : selectedRowsString });
        
        // console.log(datass);
        
        var data = [];
        var httpRequest = new XMLHttpRequest();
        var api = 'http://10.54.36.49/api-approval/public/api/onair_bts_4g_update';
        data = datass;
        httpRequest.onload = function (data) {
            console.log(data); 
        };  
        
        httpRequest.open('PUT',api,true);
        httpRequest.setRequestHeader("Content-type", "application/json; charset=utf-8");
        httpRequest.send(data);
    } 
    
    document.addEventListener('keyup',function(event){
        if (event.keyCode == 13) { 
            var data = [];
            var httpRequest = new XMLHttpRequest();
            var api = 'http://10.54.36.49/api-approval/public/api/onair_bts_4g_update';
            data = onSelectionChanged();
            httpRequest.onload = function (data) {
                console.log(data);
            };  
            
            console.log(data);
            
            httpRequest.open('PUT',api,true);
            httpRequest.setRequestHeader("Content-type", "application/json; charset=utf-8");
            httpRequest.send(data);
        } 
    }); 
    
    function onBtNext() {
        id = 0; 
        offset = limit;
        limit += 50;  
              
        console.log(limit);
        
        // setup the grid after the page has finished loading
        document.getElementById("btnNext").addEventListener('click', function() {
            document.querySelector('#myGrid');
            var httpRequest = new XMLHttpRequest();

            search = document.getElementById("search_data").value; 
            var regional_user = <?php echo json_encode($regional_user); ?>;
            if(search) { 
            var api = encodeURI('http://10.54.36.49/api-approval/public/api/onair_bts_2g');
            } else { 
            var api = encodeURI('http://10.54.36.49/api-approval/public/api/onair_bts_2g');
            }

            httpRequest.open('GET', api); 
            httpRequest.send(); 
            httpRequest.onreadystatechange = function() {
                if (httpRequest.readyState === 4 && httpRequest.status === 200) {
                    var httpResponse = JSON.parse(httpRequest.responseText);
                    gridOptions.api.setRowData(httpResponse.data);
                }
            };
        });
        
        gridOptions.api.paginationGoToNextPage();
    }
    
    function onBtPrevious() { 
        id = 0; 
        offset -= limit;
        limit -= 50;
        
        // setup the grid after the page has finished loading
        document.getElementById("btnPrevious").addEventListener('click', function() {
            document.querySelector('#myGrid');
            var httpRequest = new XMLHttpRequest();

            search = document.getElementById("search_data").value; 
            var regional_user = <?php echo json_encode($regional_user); ?>;
            if(search) { 
            var api = encodeURI('http://10.54.36.49/api-approval/public/api/onair_bts_2g');
            } else {
            var api = encodeURI('http://10.54.36.49/api-approval/public/api/onair_bts_2g');
            }

            httpRequest.open('GET', api);
            httpRequest.send();
            httpRequest.onreadystatechange = function() {
                if (httpRequest.readyState === 4 && httpRequest.status === 200) {
                    var httpResponse = JSON.parse(httpRequest.responseText);
                    gridOptions.api.setRowData(httpResponse.data);
                }
            };
        });
        
        gridOptions.api.paginationGoToPreviousPage();
    }  
    
    // function onBtnSearch() { 
    //     id = 0; 
    //     offset = limit; 
    //     limit += 50;   
    //     search = document.getElementById("search_data").value;
    //     console.log(search);   
        
    //     // setup the grid after the page has finished loading
    //     document.getElementById("btn_search").addEventListener('click', function() {  
    //         document.querySelector('#myGrid');   
    //         var httpRequest = new XMLHttpRequest();
    //         var regional_user = <?php echo json_encode($regional_user); ?>;
    //         var api = encodeURI('http://10.54.36.49/api-btsonair/public/api/onair_bts_4g?regional=' + regional_user + '&search=' + search + '&limit=' + limit + '&offset=' + offset);
    //         httpRequest.open('GET', api);     
    //         httpRequest.send(); 
    //         httpRequest.onreadystatechange = function() {
    //             if (httpRequest.readyState === 4 && httpRequest.status === 200) {
    //                 var httpResponse = JSON.parse(httpRequest.responseText);
    //                 gridOptions.api.setRowData(httpResponse.data);
    //             }
    //         };     
    //     });   
    // }   
    
    // setup the grid after the page has finished loading
    document.addEventListener('DOMContentLoaded', function() { 
        // var search = document.getElementById("search_data").value;
        var gridDiv = document.querySelector('#myGrid');
        new agGrid.Grid(gridDiv, gridOptions); 
        // do http request to get our sample data - not using any framework to keep the example self contained.
        // you will probably use a framework like JQuery, Angular or something else to do your HTTP calls.
        var httpRequest = new XMLHttpRequest();  
        var regional_user = <?php echo json_encode($regional_user); ?>;
        var api = encodeURI('http://10.54.36.49/api-approval/public/api/onair_bts_2g');
        httpRequest.open('GET', api); 
        httpRequest.send(); 
        httpRequest.onreadystatechange = function() {
            if (httpRequest.readyState === 4 && httpRequest.status === 200) {
                var httpResponse = JSON.parse(httpRequest.responseText);
                gridOptions.api.setRowData(httpResponse.data);
            }
        }; 
    });
    
</script>
<?php
} else if ($rule_user == "Project"){
?>
<script>  
    var id = 0;       
    var limit = 50;     
    var offset = 0;
    var rowData = []; 

    var columnDefs = [  
    {headerName: '', field: 'PILIH', type: 'EditableColumn', width: 50, headerCheckboxSelection: true,
        headerCheckboxSelectionFilteredOnly: true, checkboxSelection: true, filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Regional', field: 'REGIONAL', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
    // {headerName: 'Area', field: 'AREA', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Area', field: 'AREA',  type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'},
        cellEditorSelector:function (params){  
            if (params.data.AREA === 'AREA1' || params.data.AREA === 'AREA2' || params.data.AREA === 'AREA3' || params.data.AREA === 'AREA4' || params.data.AREA === "" ) return {
                component: 'agRichSelectCellEditor',
                params: {values: ['AREA1', 'AREA2','AREA3','AREA4']}
            };    
            return null;
        } 
    },
    // {headerName: 'Vendor', field: 'VENDOR', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Vendor', field: 'VENDOR',  type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'},
        cellEditorSelector:function (params){    
            if (params.data.VENDOR === 'ERICSSON' || params.data.VENDOR === 'HUAWEI' || params.data.VENDOR === 'NOKIA' || params.data.VENDOR === 'ZTE' || params.data.VENDOR === "" ) return {
                component: 'agRichSelectCellEditor', 
                params: {values: ['ERICSSON', 'HUAWEI','NOKIA','ZTE']} 
            };    
            return null;
        }                                                  
    },
    {headerName: 'EnodeB Name', field: 'ENODEB_NAME', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},                                           
    {headerName: 'Cell Name', field: 'CELL_NAME', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},                                           
    {headerName: 'Ne ID', field: 'NEID', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},                                           
    {headerName: 'Site ID', field: 'SITEID', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},                                           
    {headerName: 'TAC', field: 'TAC', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},                                           
    {headerName: 'EnodeB ID', field: 'ENODEBID', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},                                           
    {headerName: 'CI', field: 'CI', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'EARNFCN', field: 'EARNFCN', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'PID', field: 'PID', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Frequency', field: 'FREQUENCY', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Bandtype', field: 'BANDTYPE', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Bandwith', field: 'BANDWITH', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Jumlah EnodeB', field: 'JMLH_ENODEB', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Jumlah Cell', field: 'JMLH_CELL', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
    // {headerName: 'Metro E', field: 'METRO_E', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Metro E', field: 'METRO_E', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'},
        cellEditorSelector:function (params){  
            if (params.data.METRO_E === 'YES' || params.data.METRO_E === 'NO' || params.data.METRO_E === "") return {
                component: 'agRichSelectCellEditor',
                params: {values: ['YES', 'NO']}
            };     
            return null;
        }                                                    
    },
    {headerName: 'Owner Link', field: 'OWNER_LINK', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Tipe Link', field: 'TIPE_LINK', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Far End Link', field: 'FAR_END_LINK', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Total Bandwith', field: 'TOTAL_BANDWITH', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Tanggal OnAir Lease Line', field: 'TANGGAL_ONAIR_LEASE_LINE', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Site Simpul', field: 'SITE_SIMPUL', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Jumlah Site Under Simpul', field: 'JUMLAH_SITE_UNDER_SIMPUL', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Status Lokasi', field: 'STATUS_LOKASI', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Cluster Sales', field: 'CLUSTER_SALES', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Type BTS', field: 'TYPE_BTS', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Status', field: 'STATUS', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'New Existing', field: 'NEW_EXISTING', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'OnAir', field: 'ONAIR', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Date OnAir', field: 'DATE_ONAIR', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},

    {headerName: 'KPI PASS', field: 'KPI_PASS', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Date KPI PASS', field: 'DATE_KPI_PASS', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Remark', field: 'REMARK', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Departement', field: 'DEPARTEMENT', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Technical Area', field: 'TECHNICAL_AREA', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Longitude', field: 'LONGITUDE', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Latitude', field: 'LATITUDE', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Alamat', field: 'ALAMAT', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Kelurahan', field: 'KELURAHAN', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Kecamatan', field: 'KECAMATAN', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Kabupaten', field: 'KABUPATEN', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Provinsi', field: 'PROVINSI', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Tower Provider', field: 'TOWER_PROVIDER', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Nama Tower Provider', field: 'NAMA_TOWER_PROVIDER', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Status PLN', field: 'STATUS_PLN', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Vendor FMC', field: 'VENDOR_FMC', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},

    {headerName: 'Status Asset', field: 'STATUS_ASSET', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Status RAO', field: 'STATUS_RAO', width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Status Project', field: 'STATUS_PROJECT',  width: 140,filterParams: {newRowsAction: 'keep'}},
    {headerName: 'Status BTS', field: 'STATUS_BTS',  width: 140,filterParams: {newRowsAction: 'keep'}},                                               
]; 

var gridOptions = {
    defaultColDef: {
        editable: false,
        resizable: true,
        filter: true,
    },
    columnTypes: { 
        EditableColumn: {editable: true},
    },
    
    debug: true,
    rowSelection: 'multiple',
    paginationPageSize: limit,
    columnDefs: columnDefs,      
        // sideBar: true,
        paginationAutoPageSize:true,
        pagination: true,
        // suppressPaginationPanel: true,
        suppressScrollOnNewData: true, 
        // onPaginationChanged: onPaginationChanged,
        onSelectionChanged: onSelectionChanged,
        onCellEditingStopped: onCellEditingStopped,
};
    
    function setText(selector, text) {
        document.querySelector(selector).innerHTML = text;
    }   
    
    function onPaginationChanged() {
        console.log('onPaginationPageLoaded');
        
        // Workaround for bug in events order
        if (gridOptions.api) {
            setText('#lbLastPageFound', gridOptions.api.paginationIsLastPageFound());
            setText('#lbPageSize', gridOptions.api.paginationGetPageSize());
            // we +1 to current page, as pages are zero based
            setText('#lbCurrentPage', gridOptions.api.paginationGetCurrentPage() + 1);
            setText('#lbTotalPages', gridOptions.api.paginationGetTotalPages());
            
            setLastButtonDisabled(!gridOptions.api.paginationIsLastPageFound());
        }  
    } 
    
    function setLastButtonDisabled(disabled) {
        document.querySelector('#btLast').disabled = disabled;
    }
    
    function setRowData(rowData) {
        allOfTheData = rowData;
        createNewDatasource();
    }
    
    function onSelectionChanged() {
        var selectedRows = gridOptions.api.getSelectedRows();
        
        // console.log(selectedRows);
        
        var selectedRowsString = []; 
        selectedRows.forEach( function(selectedRow, index) {
            if (index!==0) {
                selectedRowsString += ', ';
            }
            selectedRowsString.push(selectedRow);
        });
        return JSON.stringify({ data : selectedRowsString });
    }

    function onEditingSelected() { 
        var selectedRows = gridOptions.api.getSelectedRows();
        
        console.log(selectedRows); 
        
        var selectedRowsString = [];
        selectedRows.forEach( function(selectedRow, index) {
            if (index!==0) {
                selectedRowsString += ', ';
            }
            selectedRowsString.push(selectedRow);
        });
        datass = JSON.stringify({ data : selectedRowsString });
        
        console.log(datass);
        
        var data = [];
        var httpRequest = new XMLHttpRequest();
        var api = 'http://10.54.36.49/api-approval/public/api/onair_bts_2g_update';
        data = datass;
        httpRequest.onload = function (data) {
            console.log(data); 
        };  
        
        httpRequest.open('PUT',api,true);
        httpRequest.setRequestHeader("Content-type", "application/json; charset=utf-8");
        httpRequest.send(data);

        alert('Insert Data Success!');
        location.reload();
    } 
    
    function onCellEditingStopped() { 
        var selectedRows = gridOptions.api.getSelectedRows();
        
        console.log(selectedRows); 
        
        // var selectedRowsString = [];
        // selectedRows.forEach( function(selectedRow, index) {
        //     if (index!==0) {
        //         selectedRowsString += ', ';
        //     }
        //     selectedRowsString.push(selectedRow);
        // });
        // datass = JSON.stringify({ data : selectedRowsString });
        
        // console.log(datass);
        
        var data = [];
        var httpRequest = new XMLHttpRequest();
        var api = 'http://10.54.36.49/api-approval/public/api/onair_bts_2g_update';
        data = datass;
        httpRequest.onload = function (data) {
            console.log(data); 
        };  
        
        httpRequest.open('PUT',api,true);
        httpRequest.setRequestHeader("Content-type", "application/json; charset=utf-8");
        httpRequest.send(data);
    } 
    
    document.addEventListener('keyup',function(event){
        if (event.keyCode == 13) { 
            var data = [];
            var httpRequest = new XMLHttpRequest();
            var api = 'http://10.54.36.49/api-approval/public/api/onair_bts_3g_update';
            data = onSelectionChanged();
            httpRequest.onload = function (data) {
                console.log(data);
            };  
            
            console.log(data);
            
            httpRequest.open('PUT',api,true);
            httpRequest.setRequestHeader("Content-type", "application/json; charset=utf-8");
            httpRequest.send(data);
        } 
    }); 
    
    function onBtNext() {
        id = 0; 
        offset = limit;
        limit += 50;  
        
        console.log(limit);
        
        // setup the grid after the page has finished loading
        document.getElementById("btnNext").addEventListener('click', function() {
            document.querySelector('#myGrid');
            var httpRequest = new XMLHttpRequest();

            search = document.getElementById("search_data").value; 
            var regional_user = <?php echo json_encode($regional_user); ?>;
            if(search) { 
                var api = encodeURI('http://10.54.36.49/api-approval/public/api/onair_bts_3g');
            } else { 
                var api = encodeURI('http://10.54.36.49/api-approval/public/api/onair_bts_3g');
            }

            httpRequest.open('GET', api); 
            httpRequest.send(); 
            httpRequest.onreadystatechange = function() {
                if (httpRequest.readyState === 4 && httpRequest.status === 200) {
                    var httpResponse = JSON.parse(httpRequest.responseText);
                    gridOptions.api.setRowData(httpResponse.data);
                }
            };
        });
        
        gridOptions.api.paginationGoToNextPage();
    }
    
    function onBtPrevious() { 
        id = 0; 
        offset -= limit;
        limit -= 50;
        
        // setup the grid after the page has finished loading
        document.getElementById("btnPrevious").addEventListener('click', function() {
            document.querySelector('#myGrid');
            var httpRequest = new XMLHttpRequest();

            search = document.getElementById("search_data").value; 
            var regional_user = <?php echo json_encode($regional_user); ?>;
            if(search) { 
                var api = encodeURI('http://10.54.36.49/api-approval/public/api/onair_bts_3g');
            } else {
                var api = encodeURI('http://10.54.36.49/api-approval/public/api/onair_bts_3g');
            }

            httpRequest.open('GET', api);
            httpRequest.send();
            httpRequest.onreadystatechange = function() {
                if (httpRequest.readyState === 4 && httpRequest.status === 200) {
                    var httpResponse = JSON.parse(httpRequest.responseText);
                    gridOptions.api.setRowData(httpResponse.data);
                }
            };
        });
        
        gridOptions.api.paginationGoToPreviousPage();
    }  
    
    // function onBtnSearch() { 
    //     id = 0; 
    //     offset = limit; 
    //     limit += 50;   
    //     search = document.getElementById("search_data").value;
    //     console.log(search);   
    
    //     // setup the grid after the page has finished loading
    //     document.getElementById("btn_search").addEventListener('click', function() {  
    //         document.querySelector('#myGrid');   
    //         var httpRequest = new XMLHttpRequest();
    //         var regional_user = <?php echo json_encode($regional_user); ?>;
    //         var api = encodeURI('http://10.54.36.49/api-btsonair/public/api/onair_bts_4g?regional=' + regional_user + '&search=' + search + '&limit=' + limit + '&offset=' + offset);
    //         httpRequest.open('GET', api);     
    //         httpRequest.send(); 
    //         httpRequest.onreadystatechange = function() {
    //             if (httpRequest.readyState === 4 && httpRequest.status === 200) {
    //                 var httpResponse = JSON.parse(httpRequest.responseText);
    //                 gridOptions.api.setRowData(httpResponse.data);
    //             }
    //         };     
    //     });   
    // }   
    var newCount = 1;
    var regional_user = <?php echo json_encode($regional_user); ?>;

    function createNewRowData() {
        var newData = {
            REGIONAL: regional_user
        };
        // newCount++;
        return newData;
    }

    function getRowData() {
        var rowData = [];
        gridOptions.api.forEachNode( function(node) {
            rowData.push(node.data);
        });
        console.log('Row Data:');
        console.log(rowData);
    }

    function clearData() {
        gridOptions.api.setRowData([]);
    }

    function onAddRow() {
        var newItem = createNewRowData();
        var res = gridOptions.api.updateRowData({add: [newItem]});
        printResult(res);
    }

    function addItems() {
        var newItems = [createNewRowData(), createNewRowData(), createNewRowData()];
        var res = gridOptions.api.updateRowData({add: newItems});
        printResult(res);
    }

    function addItemsAtIndex() {
        var newItems = [createNewRowData(), createNewRowData(), createNewRowData()];
        var res = gridOptions.api.updateRowData({add: newItems, addIndex: 2});
        printResult(res);
    }

    function updateItems() {
        // update the first 5 items
        var itemsToUpdate = [];
        gridOptions.api.forEachNodeAfterFilterAndSort( function(rowNode, index) {
            // only do first 5
            if (index>=5) { return; }

            var data = rowNode.data;
            data.price = Math.floor((Math.random()*20000) + 20000);
            itemsToUpdate.push(data);
        });
        var res = gridOptions.api.updateRowData({update: itemsToUpdate});
        printResult(res);
    }

    // onGridReady(params) {
    //     this.gridApi = params.api;
    //     this.gridColumnApi = params.columnApi;
    // }

    function onInsertRowAt2() { 
        var api = '';
        var newItem = createNewRowData();
        var res = gridOptions.api.updateRowData({ add: [newItem] });
        
        var data = {'REGIONAL':res.add[0].data.REGIONAL};
        console.log(data)
        var httpRequest = new XMLHttpRequest();
        var api = 'http://10.54.36.49/api-approval/public/api/sysinfo4g';
        console.log(api);

        $.ajax({
            url: api,
            type: 'POST',
            data: data,
            async: false,
            success: function(res){
                console.log(res);
                alert('Insert Row Success!');
                location.reload();
            }
        });

    }

    function onRemoveSelected() {
        // var selectedData = gridOptions.api.getSelectedRows();
        // var res = gridOptions.api.updateRowData({remove: selectedData});
        // printResult(res);
        var api = '';
        var selectedData = gridOptions.api.getSelectedRows();
        var res = gridOptions.api.updateRowData({ remove: selectedData });
        console.log(res.remove[0].data.id);
        var id = res.remove[0].data.id;
        var httpRequest = new XMLHttpRequest();
        var api = 'http://10.54.36.49/api-approval/public/api/deleteData/'+id;
        console.log(api);
        
        $.ajax({
            url: api,
            type: 'GET',
            async: false,
            success: function(res){
                console.log(res);
            }
        });
        alert('Remove Row Success!');
        location.reload();
    }

    // onRemoveSelected() {
    // var selectedData = this.gridApi.getSelectedRows();
    // var res = this.gridApi.updateRowData({ remove: selectedData });
    // console.log(res.remove[0].data.id);
    // var id = res.remove[0].data.id;
    // this.http.delete<any>('http://hostname/api/v1/delete/'+id).subscribe(
    //     res => {
    //       console.log(res);
    //   },
    //   (err: HttpErrorResponse) => {
    //     if (err.error instanceof Error) {
    //       console.log("Client-side error occurred.");
    //     } else {
    //       console.log("Server-side error occurred.");
    //     }
    //   });
    // }


    // onAddRow() {
    // var newItem = createNewRowData();
    // var res = this.gridApi.updateRowData({ add: [newItem] });
    
    // var data = {'name':res.add[0].data.employee_name, 'salary':res.add[0].data.employee_salary, 'age':res.add[0].data.employee_age};
    // console.log(data)
    // this.http.post<Employee>('http://hostname/api/v1/create', data).subscribe(
    //     res => {
    //       console.log(res);
    //   },
    //   (err: HttpErrorResponse) => {
    //     if (err.error instanceof Error) {
    //       console.log("Client-side error occured.");
    //     } else {
    //       console.log("Server-side error occured.");
    //     }
    //   });
    // }

    function printResult(res) {
        console.log('---------------------------------------')
        if (res.add) {
            res.add.forEach( function(rowNode) {
                console.log('Added Row Node', rowNode);
            });
        }
        if (res.remove) {
            res.remove.forEach( function(rowNode) {
                console.log('Removed Row Node', rowNode);
            });
        }
        if (res.update) {
            res.update.forEach( function(rowNode) {
                console.log('Updated Row Node', rowNode);
            });
        }
    }

    // setup the grid after the page has finished loading
    document.addEventListener('DOMContentLoaded', function() { 
        // var search = document.getElementById("search_data").value;
        var gridDiv = document.querySelector('#myGrid');
        new agGrid.Grid(gridDiv, gridOptions); 
        // do http request to get our sample data - not using any framework to keep the example self contained.
        // you will probably use a framework like JQuery, Angular or something else to do your HTTP calls.
        var httpRequest = new XMLHttpRequest();  
        var regional_user = <?php echo json_encode($regional_user); ?>;
        var api = encodeURI('http://10.54.36.49/api-approval/public/api/onair_bts_3g');
        httpRequest.open('GET', api); 
        httpRequest.send(); 
        httpRequest.onreadystatechange = function() {
            if (httpRequest.readyState === 4 && httpRequest.status === 200) {
                var httpResponse = JSON.parse(httpRequest.responseText);
                gridOptions.api.setRowData(httpResponse.data);
            }
        }; 
    });


    // wait for the document to be loaded, otherwise
    // ag-Grid will not find the div in the document.
    // document.addEventListener("DOMContentLoaded", function() {
    //     var eGridDiv = document.querySelector('#myGrid');
    //     new agGrid.Grid(eGridDiv, gridOptions);
    // });
    
</script>
<?php
}?>
</div>                                               
</div>
</div>
</div> 


</section><!-- SECTION 1 -->
</div><!-- CONTAINER-FLUID -->
</section><!-- CONTENT -->
</div><!-- CONTENT WRAPPER -->
</div><!-- WRAPPER -->
</div> <!-- BODOY --> 

<footer class="main-footer">
    <strong style="font-size: 12px">Copyright &copy; 2018 <a href="https://www.telkomsel.com">Telkomsel</a>.</strong>
</footer>
<!-- Control Sidebar -->
<!-- <aside class="control-sidebar control-sidebar-dark">
</aside> -->
<!-- /.control-sidebar -->

<!-- KUMPULAN SCRIPT  -->
<!--  -->
<script src="{{url('')}}/plugins/jquery/jquery.min.js"></script>
<script src="{{url('')}}/plugins/datatables/jquery.dataTables.js"></script>
<script src="{{url('')}}/plugins/datatables/dataTables.bootstrap4.js"></script>
<script src="{{url('')}}/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="{{url('')}}/plugins/fastclick/fastclick.js"></script>
<script src="{{url('')}}/dist/js/adminlte.min.js"></script>
<script src="{{url('')}}/dist/js/demo.js"></script>
<script src="{{url('')}}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="{{url('')}}/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="{{url('')}}/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="{{url('')}}/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="{{url('')}}/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="{{url('')}}/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="{{url('')}}/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="{{url('')}}/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{url('')}}/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="{{url('')}}/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="{{url('')}}/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="{{url('')}}/dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{url('')}}/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{url('')}}/dist/js/demo.js"></script>
<script src="{{url('')}}/plugins/chartjs-old/Chart.min.js"></script>

<script type="text/javascript">
    function logout(){
        sessionStorage.remove('token', '');
        window.location.href = "{{env('APP_URL')}}/landingPage/";
    }
</script>
<script src="{{url('')}}/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script src="{{url('')}}/plugins/jQueryUI/jquery-ui.min.js"></script>
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>

<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>

<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>


<script type="text/javascript">
    $(document).ready(function () {
        $('.navbar-light .dmenu').hover(function () {
            $(this).find('.sm-menu').first().stop(true, true).slideDown(150);
        }, function () {
            $(this).find('.sm-menu').first().stop(true, true).slideUp(105)
        });
    });
</script> 
    </body>
    </html>
    