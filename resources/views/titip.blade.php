<section class="col-lg-12 col-lg-6" id="seksion3">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="card chartcard">
                                    <div class="card-header" style="background-image: linear-gradient(-90deg, #80d0c7 , #13547a ); ">
                                        <h3 style="color: white;" class="card-title"><i class="fa fa-th mr-1"></i>Count of BTS On Air New (Region)</h3>
                                        <div class="card-tools">
                                            <button type="button" class="btn btn-tool" data-widget="collapse">
                                                <i class="fa fa-minus"></i>
                                            </button>
                                            <button type="button" class="btn btn-tool" data-widget="remove">
                                                <i class="fa fa-times"></i>
                                            </button>
                                        </div>
                                    </div><!-- CARD-HEADER -->
                                    <div class="card-body" style="background-color: #343a40;">
                                        @include('dashboardbts5')
                                    </div>
                                </div><!-- CARD -->
                            </div><!-- COL-LG-6 -->

                            <div class="col-lg-6">
                                <div class="card">
                                    <div class="card-header" style="background-image: linear-gradient(-90deg, #80d0c7 , #13547a ); ">
                                        <h3 style="color: white;" class="card-title"><i class="fa fa-th mr-1"></i>Count of BTS On Air Existing (Region)</h3>
                                        <div class="card-tools">
                                            <button type="button" class="btn btn-tool" data-widget="collapse">
                                                <i class="fa fa-minus"></i>
                                            </button>
                                            <button type="button" class="btn btn-tool" data-widget="remove">
                                                <i class="fa fa-times"></i>
                                            </button>
                                        </div>
                                    </div><!-- CARD-HEADER -->
                                    <div class="card-body" style="background-color: #343a40;">
                                        @include('dashboardbts6')
                                    </div>
                                </div><!-- CARD -->
                            </div><!-- COL-LG-6 -->
                        </div><!-- ROW -->
                    </section><!-- SECTION 3 -->