<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'HomeController@index');

// Export to csv Daily
Route::get('/bts-on-air/exportCSV','HomeController@exportBtsOnAirCSV')->name('data.exportBtsOnAirCSV');
Route::get('/resume-site/exportCSV','HomeController@exportResumeSiteCSV')->name('data.exportResumeSite');
Route::get('/resume-ne/exportCSV','HomeController@exportResumeNeCSV')->name('data.exportResumeNe');
Route::get('/resume-nodin-bts/exportCSV','HomeController@exportResumeNodinBtsCSV')->name('data.exportResumeNodinBts'); 

// Export to csv Monthly
Route::get('/bts-on-air/exportCSVMon','HomeController@exportBtsOnAirCSVMon')->name('data.exportBtsOnAirCSVMon');
Route::get('/resume-site/exportCSVMon','HomeController@exportResumeSiteCSVMon')->name('data.exportResumeSiteMon');
Route::get('/resume-ne/exportCSVMon','HomeController@exportResumeNeCSVMon')->name('data.exportResumeNeMon');
Route::get('/resume-nodin-bts/exportCSVMon','HomeController@exportResumeNodinBtsCSVMon')->name('data.exportResumeNodinBtsMon'); 

// Export Menu BaseLine BTS ON AIR 
Route::get('/baseline/exportCSVbtsonair4g','HomeController@exportCSVbtsonair4g')->name('data.exportCSVbtsonair4g');
Route::get('/baseline/exportCSVbtsonair3g','HomeController@exportCSVbtsonair3g')->name('data.exportCSVbtsonair3g');
Route::get('/baseline/exportCSVbtsonair2g','HomeController@exportCSVbtsonair2g')->name('data.exportCSVbtsonair2g');

// template kosong 
Route::get('/template/sysinfo4g','HomeController@templatesysinfo4g')->name('data.exportCSVbtsonair4g');
Route::get('/template/sysinfo3g','HomeController@templatesysinfo3g')->name('data.exportCSVbtsonair3g');
Route::get('/template/sysinfo2g','HomeController@templatesysinfo2g')->name('data.exportCSVbtsonair2g');
  
// Import Menu BaseLine BTS ON AIR  
Route::post('/import/btsonair4gimport', 'HomeController@storeDataBtsOnAir4g');
Route::post('/import/btsonair3gimport', 'HomeController@storeDataBtsOnAir3g');
Route::post('/import/btsonair2gimport', 'HomeController@storeDataBtsOnAir2g');
 
//getData to Menu Nodin & Mom
Route::get('/data-nodin-mom', 'NodinMomController@index')->name('view.nodin');
Route::get('/data-nodin/getJsonDataNodin','NodinMomController@getJsonDataNodin')->name('data.getJsonDataNodin');
Route::get('/data-mom/getJsonDataMom','NodinMomController@getJsonDataMom')->name('data.getJsonDataMom');
 
//generate pdf
Route::get('/generate-pdf-nodin/{date}','NodinMomController@generatePdfNodin')->name('generate.pdf.nodin');
Route::get('/generate-pdf-mom/{date}','NodinMomController@generatePdfMom')->name('generate.pdf.mom');

Auth::routes(); 

Route::get('/logout', 'Auth\LoginController@logout');

//tab dashboard
Route::get('/', 'HomeController@index');

Route::get('monthly', 'HomeController@monthly'); 

Route::get('baseline4g', 'HomeController@baseline'); 
Route::post('/getJsonBts4g','DatatableController@getJsonDataBts4g')->name('getJsonBts4g');

Route::get('baseline3g', 'HomeController@baseline3g'); 
Route::post('/getJsonBts3g','DatatableController@getJsonDataBts3g')->name('getJsonBts3g');

Route::get('baseline2g', 'HomeController@baseline2g'); 
Route::post('/getJsonBts2g','DatatableController@getJsonDataBts2g')->name('getJsonBts2g'); 
  
// Auth::routes();

// Route::get('/home', 'HomeController@index');
Route::get('dafinci', 'ApiController@dafinci');
Route::get('remedy', 'ApiController@remedy');
Route::get('btsonair', 'ApiController@btsonair');
Route::get('nodin', 'ApiController@nodin');
Route::get('add_license', 'ApiController@add_license');
Route::get('act_cell', 'ApiController@act_cell');
Route::get('dea_cell', 'ApiController@dea_cell');
Route::get('act_license', 'ApiController@act_license');
Route::get('dea_license', 'ApiController@dea_license');
Route::get('chart_btsonair', 'ApiController@chart_btsonair');
Route::get('chart_nodin', 'ApiController@chart_nodin');
Route::get('chart_user', 'ApiController@chart_user');
Route::get('chart_dea', 'ApiController@chart_dea');
Route::get('add_bts', 'ApiController@add_bts');
Route::get('chart_rekon', 'ApiController@chart_rekon');
Route::get('cell_deactivated', 'ApiController@cell_deactivated');
Route::get('test', 'ApiController@test');

Route::get('neidkosong', 'ApiController@neidkosong');
Route::get('lackosong', 'ApiController@lackosong');
Route::get('cikosong', 'ApiController@cikosong');
Route::get('enodebid', 'ApiController@enodebid');
Route::get('datasalah', 'ApiController@datasalah');
Route::get('wrong_stylo', 'ApiController@wrong_stylo');

Route::get('traffic_lac', 'ApiController@traffic_lac');
Route::get('traffic_ci', 'ApiController@traffic_ci');

Route::get('index2', 'ApiController@index2');
Route::get('index', 'ApiController@index');

//bts on air for chart
Route::get('btsonair2g', 'ApiController@btsonair2g');
Route::get('btsonair3g', 'ApiController@btsonair3g');
Route::get('btsonair4g', 'ApiController@btsonair4g');

//data resume nodin bts
Route::get('data_resume_nodin_bts', 'ApiController@data_resume_nodin_bts');
