<?php header('Access-Control-Allow-Origin: *'); ?>
<!DOCTYPE html> 
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">  
    <title>NEISA | DASHBOARD</title>
    <!-- Tell the browser to be responsive to screen width -->
    
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <!--  -->
    <link rel="stylesheet" href="https://unpkg.com/material-components-web@1.0.1/dist/material-components-web.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"> 
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/dist/css/adminlte.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/plugins/morris/morris.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/plugins/datatables/dataTables.bootstrap4.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/dist/css/adminlte.min.css">
    <link rel="stylesheet" href="<?php echo e(url('')); ?>/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    
    <script> var __basePath = ''; </script>
    <style> html, body { margin: 0; padding: 0; height: 100%; } </style>
        <script src="https://unpkg.com/ag-grid-enterprise@20.2.0/dist/ag-grid-enterprise.min.js"></script>    
        <script src="https://unpkg.com/ag-grid-community@20.2.0/dist/ag-grid-community.min.js"></script> 
    
    <!-- *** START TAMBAH IMPORT FONTAWESOME IYON *** -->
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('')); ?>/plugins/fontawesome-free-5.6.3-web/css/fontawesome.css">
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('')); ?>/plugins/fontawesome-free-5.6.3-web/css/solid.css">
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('')); ?>/plugins/fontawesome-free-5.6.3-web/css/brands.css">
    <!-- *** END TAMBAH IMPORT FONTAWESOME IYON *** -->
    
    <style>
        /* ** START TAMBAH CSS IYON** */
        .dropClass {
            /* display: none; */
            visibility: hidden;
            opacity: 0;
            transition:
            all .5s ease;
            background-color: transparent;
            min-width: 160px;
            overflow: auto;
            z-index: 1;
            height: 0;
            /* margin-bottom: -5%; */
        }
        .dropClass a {
            color: black;
            padding: 12px 16px;
            /* text-decoration: none; */
            display: block;
        }
        .dropClass a:hover {background-color: rgba(255,255,255,.1);}
        .show {
            /* display: block; */
            visibility: visible;
            opacity: 1;
            height: auto;
            padding: 0.5rem 1rem;
            background-color: rgba(255,255,255,.3);
            margin-bottom: 1.5%;
            border-radius: 3%;
        }
        .putar {
            transform: rotate(90deg);
            transition: all .5s ease;
        }
        .brand-image {
            line-height: .8;
            max-height: 53px;
            width: auto;
            margin-left: 0.7rem;
            margin-right: .5rem;
            margin-top: -3px;
            float: none;
            opacity: .9;
        }
        .backgroundImg {
            width: auto;
            height: 100%;
            opacity: 1;
            position: absolute;
        }
        .backgroundImg2 {
            position: fixed;
            width: 100%;
            max-height: 56px;
            margin-left: -2%;
            opacity: 1;
        }
        .nav-item:hover {
            background-color: rgba(255,255,255,.3);
            border-radius: 5%;
            transition: all .2s ease;
        }
        .active {
            background-color: rgba(243, 255, 226, .8) !important;
            color: #343a40 !important;
            font-weight: 600;
        }
        .berisik {
            min-height:500px !important
        }
        .tesDiv {
            z-index: -1;
            opacity: .4;
            background: url(./dist/img/tesblek.png) center center
        }
        .tesDiv .bekgron{
            z-index: 1;
            opacity: 1
        }
        /* ** END TAMBAH IYON** */
        /* ** START UI BARU** */
        #bungkus {
            /* background: url(./dist/img/darkwall6.jpg) center center; */
            background-image:linear-gradient(to right, #0F2027,#203A43,#2C5364, #203A43, #0F2027);
        }
        .garisijo {
            background-color: rgba(150, 178, 138, 1);
            height: 3px;
            width: 100%;
            position: absolute;
            bottom: 0;
            left: 0;
            /* margin-left: -6%; */
        }
        .teksboks {
            width: 85%;
            position: absolute;
            bottom: 0;
            left: 0;
        }
        .teksne{
            /* bottom: 0;
            right: 3%; */
            margin:auto;
            position: absolute;
        }
        .boksHead {
            font-size: 30px;
            /* box-shadow: 0 3px 1px 0 rgba(0, 0, 0, 0.2), 0 1px 0px 0 rgba(0, 0, 0, 0.19); */
            padding: 10px;
            font-weight: 500;
            border-radius: 0;
            background-color: rgba(255, 255, 255, .2);
            color: #96b28a;
            font-weight: bold;
            box-shadow: none;
            margin-bottom:0 !important;
        }
        .boksBody {
            height: 90px;
            /* background-color: #343a40; */
            border-radius: 0;
            background-color: rgba(255, 255, 255, .2);
            /* background-image: linear-gradient(to top, rgba(0,255,255,.2), rgba(150, 178, 138, .5)); */
            /* background-color: rgba(0, 0, 0, .5); */
            box-shadow: none;
        }
        .inner {
            padding:0 !important;
        }
        .card.chartcard {
            background-color:transparent;
            border: 0;
            border-radius: 0;
            box-shadow: none;
        }
        table {
            font-size: 14px;
            background-color: rgba(1, 14, 23,.9)!important;
            color:lightgrey;
        }
        /* tr:hover {
            background-color: rgba(255, 255, 255, .2)
        } */
        tr > td:hover {
            background-color: rgba(1, 14, 23,.9)!important;
            color:lightgrey;
        }
        
        .table td, .table th {
            border: 1px solid #ddd;
        }
        
        thead {
            background-color: rgba(1, 14, 23,.9)!important;
            color:lightgrey;
        }
        .kepanjangan {
            font-size: 10px;
        }
        .kepanjangantot {
            font-size: 12px;
        }
        /* ** END UI BARU** */
        
        .page-link {
            color: #fff;
            background-color: #262c2e;
        }
        
        .social-part .fa{
            padding-right:20px;
        }
        ul li a{
            margin-right: 20px;
        }
        
        .bg-light {
            color:#fff;
            background-image:linear-gradient(to right, #1c1f20, #2d3436, #2d3436, #2d3436);
        } 
        
        .bg-light, .bg-light a {
            color: #fff!important;
        }
        
        .dropdown-menu {
            background-color: #262c2e;
            color: #0F2027;
        }
        
        .active1 {
            background-color: #6c7173; !important;
            color: #343a40 !important;
            font-weight: 600;
        }
        
        .item:hover {
            background-color: #0F2027;
        }
        
        .invisible {
            visibility: hidden;
        }

        .dropdown-item:focus, .dropdown-item:hover {
            background-color: #6c7173;
        }

        .form-control2 {
            font-size: 16px !important;
            background-color:transparent !important;
            border:none !important;
            color: #fff !important;
            border-radius: 0 !important;
            padding: 0 !important;
            padding-left: 4px !important;
            padding-top: 0px !important;
            padding-bottom: 0px !important;
            background: #000;
        }
        
    </style>
    
</head>
<body class="hold-transition sidebar-mini " style="background: #f4f6f9; color: white;">
    <div class="wrapper  ">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand bg-dark" style="margin-left: 250px; position:fixed; width:100%;">
            <img src="<?php echo e(url('')); ?>/dist/img/wall5.jpg" class="backgroundImg2" style="position: fixed;width: 100%;">
            <!--  -->
                <!-- Left navbar links -->
                <ul class="navbar-nav" style="z-index: 999;">
                    <li class="nav-item">
                        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars nav-button" style="color:white;"></i></a>
                    </li>
                    <li class=" navbar-brand" style="color:white; margin-left: 10%;">NEISA | DASHBOARD</li>
                    <li class="nav-item">
                        <a class="nav-link btn btn-lg" href="http://10.54.36.49/landingPage/" onclick="sessionStorage.clear();" style="
                        color: #343a40 !important;
                        background-color: #fff; 
                        position: fixed;
                        font-size: 10px;
                        right: 1%;
                        height: auto;
                        box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 10px 0 rgba(0,0,0,.12);
                        text-transform: uppercase;
                        font-family: Roboto;
                        padding: 1%;"><i class="fa fa-sign-out-alt"></i> Log Out</a>
                    </li>
                </ul>
            </nav>
            
            <!-- Main Sidebar Container -->
            <aside class="main-sidebar sidebar-dark-primary elevation-4 berisik">
                <img src="<?php echo e(url('')); ?>/dist/img/wall3.jpg" class="backgroundImg">
                <!--  -->
                <!-- Brand Logo -->
                <a href="#" class="brand-link">
                    <img src="<?php echo e(url('')); ?>/dist/img/tsel-white.png"
                    style="opacity: .8; float:none; widht:200px; line-height:.8; max-height:53px;margin-left:0.7rem;margin-right:.5rem;margin-top:-3px">
                </a>
                
                <!-- Sidebar -->
                <div class="sidebar">
                    <!-- Sidebar user panel (optional) -->
                    <!-- Sidebar Menu -->
                    <nav class="mt-2">
                        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                            <li class="nav-item">
                                <a href="http://10.54.36.49/dashboard-bts-on-air/public/" class="nav-link aa active" style="color: #fff;padding: 0.5rem 1rem !important;">
                                    <i class="nav-icon fa fa-home"></i>
                                    <p style="margin-left: 3px;">Dashboard</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="http://10.54.36.49/dashboard-license" class="nav-link aa " style="color: #fff;padding: 0.5rem 1rem !important;">
                                    <i class="nav-icon fa fa-newspaper"></i>
                                    <p style="margin-left: 3px;">License</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="http://10.54.36.49/btsonair" class="nav-link " style="color: #fff;padding: 0.5rem 1rem !important;">
                                    <i class="nav-icon fa fa-broadcast-tower"></i>
                                    <p style="margin-left: 3px;">BTS Status</p>
                                </a>
                            </li>
                            
                            <li class="nav-item" style="cursor: pointer;">
                                <a onclick="dropDead()" class="nav-link aa dropbtn" style="color: #fff;padding: 0.5rem 1rem !important;">
                                    <i class="nav-icon fa fa-angle-right" id="dropIcon"></i>
                                    <p style="margin-left: 3px;">Create</p>
                                </a>
                            </li>
                            <div class="dropClass" id="dropId">
                                <li class="nav-item">
                                    <a href="http://10.54.36.49/apk-nodin/index.php/NodinController" class="nav-link aa" style="color: #fff;padding: 0.5rem 1rem !important;">
                                        <i class="nav-icon fa fa-list-alt"></i>
                                        <p style="margin-left: 3px;">Create Integrasi</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="http://10.54.36.49/apk-nodin-stylo/index.php/NodinController" class="nav-link aa" style="color: #fff;padding: 0.5rem 1rem !important;">
                                        <i class="nav-icon fa fa-list-alt"></i>
                                        <p style="margin-left: 3px;">Create Rehoming</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="http://10.54.36.49/apk-nodin-dismantle/index.php/NodinController" class="nav-link aa " style="color: #fff;padding: 0.5rem 1rem !important;">
                                        <i class="nav-icon fa fa-list-alt"></i>
                                        <p style="margin-left: 3px;">Create Dismantle</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="http://10.54.36.49/apk-nodin-relocation/index.php/NodinController" class="nav-link aa " style="color: #fff;padding: 0.5rem 1rem !important;">
                                        <i class="nav-icon fa fa-list-alt"></i>
                                        <p style="margin-left: 3px;">Create Relocation</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="http://10.54.36.49/apk-nodin-swap/index.php/NodinController" class="nav-link aa" style="color: #fff;padding: 0.5rem 1rem !important;">
                                        <i class="nav-icon fa fa-list-alt"></i>
                                        <p style="margin-left: 3px;">Create Swap</p>
                                    </a>
                                </li>
                            </div>
                            <li class="nav-item">
                                <a href="http://10.54.36.49/change-front-2/public/" class="nav-link aa " style="color: #fff;padding: 0.5rem 1rem !important;">
                                    <i class="nav-icon fa fa-project-diagram"></i>
                                    <p style="margin-left: 3px;">Process Tracking</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="http://10.54.36.49/tableList" class="nav-link aa" style="color: #fff;padding: 0.5rem 1rem !important;">
                                    <i class="nav-icon fa fa-table"></i>
                                    <p style="margin-left: 3px;">Nodin & MoM Report</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="http://10.54.36.49/apk-report/index.php/ReportController" class="nav-link aa " style="color: #fff;padding: 0.5rem 1rem !important;">
                                    <i class="nav-icon fa fa-book"></i>
                                    <p style="margin-left: 3px;">Report Remedy</p>
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <!-- /.sidebar-menu -->
                </div>
                <!-- /.sidebar -->
            </aside>
            
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper" id="bungkus" style="margin-top:55px;">

                    <nav class="navbar navbar-expand-sm navbar-light bg-light">
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                            
                            <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                                <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo e(url('/')); ?>">Daily </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo e(url('monthly')); ?>">Monthly</a>
                                    </li>
                                    <li class="nav-item dropdown dmenu">
                                        <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                                            Baseline
                                        </a>
                                        <div class="dropdown-menu sm-menu">
                                                <a class="dropdown-item " href="<?php echo e(url('baseline4g')); ?>">Baseline 4G</a>
                                                <a class="dropdown-item " href="<?php echo e(url('baseline3g')); ?>">Baseline 3G</a>
                                                <a class="dropdown-item active1" href="<?php echo e(url('baseline2g')); ?>">Baseline 2G</a>
                                        </div>
                                    </li>
                                </div>
                            </nav>  

                <section class="content" >
                    <div class="container-fluid" >
                        <section class="col-lg-12" id="seksion1">
                            
                            <div class="box box-primary">

                                <b><h5 style="text-align:center; padding-top: 15px;">ON AIR BTS 2G</h5></b>

                                <?php if(session('success')): ?>
                                <div style="margin-top:10px" class="alert alert-success alert-dismissible">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        Import Data Success 
                                </div>
                                <?php endif; ?> 
                                <?php if(session('error')): ?> 
                                <div style="margin-top:10px" class="alert alert-danger alert-dismissible">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        Import Data Failed
                                </div>
                                <?php endif; ?> 

                                <div class="box-body">
                                    <div class="row"> 
                                        <div class="col-md-12">
                                                    
                                                

                                                    <div class="row " style="margin-top: 5px; margin-bottom: 10px; margin-left:2px;">
                                                        <a href="<?php echo e(url('baseline/exportCSVbtsonair2g')); ?>" class="btn btn-primary btn-sm" role="button">Export To Excel </a>                                                   
                                                    </div>
                                                    <div id="grid-wrapper" style="width:100%">
    
                                                    <div style="height: 350px;" id="myGrid" class="ag-theme-balham-dark"> </div> 
                                                    </div>

                                                    <div style="margin-top:10px" class="row">
                                                            <div class="col-5">
                                                                        <form action="<?php echo e(url('/import/btsonair2gimport')); ?>" method="post" enctype="multipart/form-data">
                                                                            <?php echo csrf_field(); ?>
                                                                            <div>
                                                                                  
                                                                                <div style="" class="btn-group" role="group"> 
                                                                  
                                                                                    <button class="btn btn-primary btn-sm">Submit</button>
                        
                                                                                    <input style="background:#000" type="file" class="form-control2" name="file_import">
                                                                                    <p class="text-danger"><?php echo e($errors->first('file')); ?></p>
                                                                                </div>
                         
                                                                            </div> 
                                                                        </form>
         
                                                                </div>
                                                                <div class="col-3"></div>
                                                                <div class="col-4">
                                                                        <a style="right:0; margin-right:6px; position: absolute;" href="<?php echo e(url('/template/sysinfo2g')); ?>" class="btn btn-primary btn-sm" role="button">Download Template</a>  
                                                                </div> 
                                                        </div>                                         
                                            
                                            <div style="margin-top: 5px"> 
                                                    <span class="label invisible">Last Page Found:</span>
                                                    <span class="value invisible" id="lbLastPageFound">-</span> 
                                                    <span class="label invisible">Page Size:</span>
                                                    <span class="value invisible" id="lbPageSize">-</span>
                                                    <span class="label invisible">Total Pages:</span>
                                                    <span class="value invisible" id="lbTotalPages">-</span>
                                                    <span class="label invisible">Current Page:</span>
                                                    <span class="value invisible" id="lbCurrentPage">-</span>
                                                </div> 
                                          
                                                               
                            
                                            <script>
                                                var id = 0;       
                                                var limit = 50;      
                                                var offset = 0;     
                                                var rowData = [];
                                                var rule_user = <?php echo json_encode($rule_user); ?>;
                                                
                                                if(rule_user == "Radio"){    

                                                var columnDefs = [
                                                {headerName: 'CELL NAME', field: 'CELL_NAME', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'NE ID', field: 'NEID', type: 'EditableColumn', width: 140, filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'SITE ID', field: 'SITEID', type: 'EditableColumn', width: 140, filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'REGIONAL', field: 'REGIONAL', type: 'EditableColumn', width: 140, suppressSizeToFit: true,filterParams: {newRowsAction: 'keep'},
                                                cellEditorSelector:function (params){  
                                                    if (params.data.REGIONAL === 'REGIONAL1' || params.data.REGIONAL === 'REGIONAL2' || params.data.REGIONAL === 'REGIONAL3' ||
                                                        params.data.REGIONAL === 'REGIONAL4' || params.data.REGIONAL === 'REGIONAL5' || params.data.REGIONAL === 'REGIONAL6' ||
                                                        params.data.REGIONAL === 'REGIONAL7' || params.data.REGIONAL === 'REGIONAL8' || params.data.REGIONAL === 'REGIONAL9' ||
                                                        params.data.REGIONAL === 'REGIONAL10' || params.data.REGIONAL === 'REGIONAL11' || params.data.REGIONAL === ""
                                                    ) return {
                                                        component: 'agRichSelectCellEditor', 
                                                        params: {values: ['REGIONAL1', 'REGIONAL2', 'REGIONAL3', 'REGIONAL4', 'REGIONAL5', 'REGIONAL6', 'REGIONAL7', 'REGIONAL8', 'REGIONAL9', 'REGIONAL10', 'REGIONAL11']}
                                                    };     
                                                    return null;                     }                                                     
                                                },
                                                {headerName: 'AREA', field: 'AREA', type: 'EditableColumn', width: 140, filterParams: {newRowsAction: 'keep'},
                                                cellEditorSelector:function (params){ 
                                                    if (params.data.AREA === 'AREA1' || params.data.AREA === 'AREA2' || params.data.AREA === 'AREA3' || params.data.AREA === 'AREA4' || params.data.AREA === "" ) return {
                                                        component: 'agRichSelectCellEditor',
                                                        params: {values: ['AREA1', 'AREA2','AREA3','AREA4']}
                                                    };   
                                                    return null;
                                                                                    }                                                 
                                                },
                                                {headerName: 'VENDOR', field: 'VENDOR', type: 'EditableColumn', width: 140, filterParams: {newRowsAction: 'keep'},
                                                cellEditorSelector:function (params){    
                                                    if (params.data.VENDOR === 'ERICSSON' || params.data.VENDOR === 'HUAWEI' || params.data.VENDOR === 'NOKIA' || params.data.VENDOR === 'ZTE' || params.data.VENDOR === "" ) return {
                                                        component: 'agRichSelectCellEditor', 
                                                        params: {values: ['ERICSSON', 'HUAWEI','NOKIA','ZTE']} 
                                                    };    
                                                    return null;
                                                                                    }                                                  
                                                },
                                                {headerName: 'BSC Name', field: 'BSC_NAME', type: 'EditableColumn', width: 140, filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'SITE NAME', field: 'SITE_NAME', type: 'EditableColumn', width: 140, filterParams: {newRowsAction: 'keep'}}, 
                                                {headerName: 'LAC', field: 'LAC', type: 'EditableColumn', width: 140, filterParams: {newRowsAction: 'keep'}}, 
                                                {headerName: 'CELL ID', field: 'CELL_ID', type: 'EditableColumn', width: 140, filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'BTS NUMBER', field: 'BTSNUMBER', type: 'EditableColumn', width: 140, filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'CELL NUMBER', field: 'CELLNUMBER', type: 'EditableColumn', width: 140, filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'FREQUENCY', field: 'FREQUENCY', type: 'EditableColumn', width: 140, filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'BANDTYPE', field: 'BANDTYPE', type: 'EditableColumn', width: 140, filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'NCC', field: 'NCC', type: 'EditableColumn', width: 140, filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'BCC', field: 'BCC', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F0', field: 'f0', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F1', field: 'f1', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F2', field: 'f2', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F3', field: 'f3', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F4', field: 'f4', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F5', field: 'f5', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F6', field: 'f6', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F7', field: 'f7', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F8', field: 'f8', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F9', field: 'f9', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F10', field: 'f10', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F11', field: 'f11', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}}, 
                                                {headerName: 'TRX SEC', field: 'Trx_Sec', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'TRX SITE', field: 'Trx_Site', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'JUMLAH SITE', field: 'JMLH_SITE', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'JUMLAH BTS', field: 'JMLH_BTS', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'JUMLAH CELL', field: 'JMLH_CELL', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'JUMLAH BSC', field: 'JMLH_BSC', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'METRO E', field: 'METRO_E', width: 140,filterParams: {newRowsAction: 'keep'},
                                                cellEditorSelector:function (params){  
                                                    if (params.data.METRO_E === 'YES' || params.data.METRO_E === 'NO' || params.data.METRO_E === "") return {
                                                        component: 'agRichSelectCellEditor',
                                                        params: {values: ['YES', 'NO']}
                                                    };     
                                                    return null;
                                                                                    }                                                
                                                },
                                                {headerName: 'OWNER LINK', field: 'OWNER_LINK', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'TIPE LINK', field: 'TIPE_LINK', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'FAR END LINK', field: 'FAR_END_LINK', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'TOTAL BANDWITH', field: 'TOTAL_BANDWITH', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'TANGGAL ONAIR LEASE LINE', field: 'TANGGAL_ONAIR_LEASE_LINE', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'SITE SIMPUL', field: 'SITE_SIMPUL', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'JUMLAH SITE UNDER SIMPUL', field: 'JUMLAH_SITE_UNDER_SIMPUL', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'STATUS LOKASI', field: 'STATUS_LOKASI', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'CLUSTER SALES', field: 'CLUSTER_SALES', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'TYPE BTS', field: 'TYPE_BTS', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'STATUS', field: 'STATUS', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'TYPE FREQ', field: 'TYPE_FREQ', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'NEW EXISTING', field: 'NEW_EXISTING', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'ONAIR', field: 'ONAIR', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'DATE ONAIR', field: 'DATE_ONAIR', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'KPI PASS', field: 'KPI_PASS', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'DATE KPI PASS', field: 'DATE_KPI_PASS', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'REMARK', field: 'REMARK', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'DEPARTEMENT', field: 'DEPARTEMENT', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'TECHNICAL AREA', field: 'TECHNICAL_AREA', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'LONGITUDE', field: 'LONGITUDE', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'LATITUDE', field: 'LATITUDE', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'ALAMAT', field: 'ALAMAT', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'KELURAHAN', field: 'KELURAHAN', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'KECAMATAN', field: 'KECAMATAN', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'KABUPATEN KOTA', field: 'KABUPATEN_KOTA', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'PROVINSI', field: 'PROVINSI', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'TOWER PROVIDER', field: 'TOWER_PROVIDER', width: 180,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'NAMA TOWER PROVIDER', field: 'NAMA_TOWER_PROVIDER', width: 200,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'STATUS PLN', field: 'STATUS_PLN', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'VENDOR FMC', field: 'VENDOR_FMC', type: 'EditableColumn', width: 250,filterParams: {newRowsAction: 'keep'}},
                                                ];  

                                                } else if(rule_user == "Transmisi") {

                                                var columnDefs = [
                                                {headerName: 'CELL NAME', field: 'CELL_NAME', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'NE ID', field: 'NEID', width: 140, filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'SITE ID', field: 'SITEID', width: 140, filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'REGIONAL', field: 'REGIONAL', width: 140, suppressSizeToFit: true,filterParams: {newRowsAction: 'keep'},
                                                cellEditorSelector:function (params){  
                                                    if (params.data.REGIONAL === 'REGIONAL1' || params.data.REGIONAL === 'REGIONAL2' || params.data.REGIONAL === 'REGIONAL3' ||
                                                        params.data.REGIONAL === 'REGIONAL4' || params.data.REGIONAL === 'REGIONAL5' || params.data.REGIONAL === 'REGIONAL6' ||
                                                        params.data.REGIONAL === 'REGIONAL7' || params.data.REGIONAL === 'REGIONAL8' || params.data.REGIONAL === 'REGIONAL9' ||
                                                        params.data.REGIONAL === 'REGIONAL10' || params.data.REGIONAL === 'REGIONAL11' || params.data.REGIONAL === ""
                                                    ) return {
                                                        component: 'agRichSelectCellEditor', 
                                                        params: {values: ['REGIONAL1', 'REGIONAL2', 'REGIONAL3', 'REGIONAL4', 'REGIONAL5', 'REGIONAL6', 'REGIONAL7', 'REGIONAL8', 'REGIONAL9', 'REGIONAL10', 'REGIONAL11']}
                                                    };     
                                                    return null;                     }                                                     
                                                },
                                                {headerName: 'AREA', field: 'AREA', width: 140, filterParams: {newRowsAction: 'keep'},
                                                cellEditorSelector:function (params){ 
                                                    if (params.data.AREA === 'AREA1' || params.data.AREA === 'AREA2' || params.data.AREA === 'AREA3' || params.data.AREA === 'AREA4' || params.data.AREA === "" ) return {
                                                        component: 'agRichSelectCellEditor',
                                                        params: {values: ['AREA1', 'AREA2','AREA3','AREA4']}
                                                    };   
                                                    return null;
                                                                                    }                                                 
                                                },
                                                {headerName: 'VENDOR', field: 'VENDOR', width: 140, filterParams: {newRowsAction: 'keep'},
                                                cellEditorSelector:function (params){    
                                                    if (params.data.VENDOR === 'ERICSSON' || params.data.VENDOR === 'HUAWEI' || params.data.VENDOR === 'NOKIA' || params.data.VENDOR === 'ZTE' || params.data.VENDOR === "" ) return {
                                                        component: 'agRichSelectCellEditor', 
                                                        params: {values: ['ERICSSON', 'HUAWEI','NOKIA','ZTE']} 
                                                    };    
                                                    return null;
                                                                                    }                                                  
                                                },
                                                {headerName: 'BSC Name', field: 'BSC_NAME', width: 140, filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'SITE NAME', field: 'SITE_NAME', width: 140, filterParams: {newRowsAction: 'keep'}}, 
                                                {headerName: 'LAC', field: 'LAC', width: 140, filterParams: {newRowsAction: 'keep'}}, 
                                                {headerName: 'CELL ID', field: 'CELL_ID', width: 140, filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'BTS NUMBER', field: 'BTSNUMBER', width: 140, filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'CELL NUMBER', field: 'CELLNUMBER', width: 140, filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'FREQUENCY', field: 'FREQUENCY', width: 140, filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'BANDTYPE', field: 'BANDTYPE', width: 140, filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'NCC', field: 'NCC', width: 140, filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'BCC', field: 'BCC', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F0', field: 'f0', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F1', field: 'f1', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F2', field: 'f2', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F3', field: 'f3', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F4', field: 'f4', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F5', field: 'f5', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F6', field: 'f6', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F7', field: 'f7', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F8', field: 'f8', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F9', field: 'f9', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F10', field: 'f10', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F11', field: 'f11', width: 140,filterParams: {newRowsAction: 'keep'}}, 
                                                {headerName: 'TRX SEC', field: 'Trx_Sec', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'TRX SITE', field: 'Trx_Site', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'JUMLAH SITE', field: 'JMLH_SITE', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'JUMLAH BTS', field: 'JMLH_BTS', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'JUMLAH CELL', field: 'JMLH_CELL', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'JUMLAH BSC', field: 'JMLH_BSC', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'METRO E', field: 'METRO_E', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'},
                                                cellEditorSelector:function (params){  
                                                    if (params.data.METRO_E === 'YES' || params.data.METRO_E === 'NO' || params.data.METRO_E === "") return {
                                                        component: 'agRichSelectCellEditor',
                                                        params: {values: ['YES', 'NO']}
                                                    };     
                                                    return null;
                                                                                    }                                                
                                                },
                                                {headerName: 'OWNER LINK', field: 'OWNER_LINK', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'TIPE LINK', field: 'TIPE_LINK', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'FAR END LINK', field: 'FAR_END_LINK', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'TOTAL BANDWITH', field: 'TOTAL_BANDWITH', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'TANGGAL ONAIR LEASE LINE', field: 'TANGGAL_ONAIR_LEASE_LINE', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'SITE SIMPUL', field: 'SITE_SIMPUL', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'JUMLAH SITE UNDER SIMPUL', field: 'JUMLAH_SITE_UNDER_SIMPUL', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'STATUS LOKASI', field: 'STATUS_LOKASI', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'CLUSTER SALES', field: 'CLUSTER_SALES', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'TYPE BTS', field: 'TYPE_BTS', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'STATUS', field: 'STATUS', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'TYPE FREQ', field: 'TYPE_FREQ', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'NEW EXISTING', field: 'NEW_EXISTING', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'ONAIR', field: 'ONAIR', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'DATE ONAIR', field: 'DATE_ONAIR', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'KPI PASS', field: 'KPI_PASS', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'DATE KPI PASS', field: 'DATE_KPI_PASS', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'REMARK', field: 'REMARK', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'DEPARTEMENT', field: 'DEPARTEMENT', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'TECHNICAL AREA', field: 'TECHNICAL_AREA', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'LONGITUDE', field: 'LONGITUDE', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'LATITUDE', field: 'LATITUDE', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'ALAMAT', field: 'ALAMAT', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'KELURAHAN', field: 'KELURAHAN', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'KECAMATAN', field: 'KECAMATAN', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'KABUPATEN KOTA', field: 'KABUPATEN_KOTA', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'PROVINSI', field: 'PROVINSI', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'TOWER PROVIDER', field: 'TOWER_PROVIDER', width: 180,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'NAMA TOWER PROVIDER', field: 'NAMA_TOWER_PROVIDER', width: 200,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'STATUS PLN', field: 'STATUS_PLN', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'VENDOR FMC', field: 'VENDOR_FMC', width: 250,filterParams: {newRowsAction: 'keep'}},
                                                ];  

                                                } else if(rule_user == "Tower") {

                                                var columnDefs = [
                                                {headerName: 'CELL NAME', field: 'CELL_NAME', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'NE ID', field: 'NEID', width: 140, filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'SITE ID', field: 'SITEID', width: 140, filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'REGIONAL', field: 'REGIONAL', width: 140, suppressSizeToFit: true,filterParams: {newRowsAction: 'keep'},
                                                cellEditorSelector:function (params){  
                                                    if (params.data.REGIONAL === 'REGIONAL1' || params.data.REGIONAL === 'REGIONAL2' || params.data.REGIONAL === 'REGIONAL3' ||
                                                        params.data.REGIONAL === 'REGIONAL4' || params.data.REGIONAL === 'REGIONAL5' || params.data.REGIONAL === 'REGIONAL6' ||
                                                        params.data.REGIONAL === 'REGIONAL7' || params.data.REGIONAL === 'REGIONAL8' || params.data.REGIONAL === 'REGIONAL9' ||
                                                        params.data.REGIONAL === 'REGIONAL10' || params.data.REGIONAL === 'REGIONAL11' || params.data.REGIONAL === ""
                                                    ) return {
                                                        component: 'agRichSelectCellEditor', 
                                                        params: {values: ['REGIONAL1', 'REGIONAL2', 'REGIONAL3', 'REGIONAL4', 'REGIONAL5', 'REGIONAL6', 'REGIONAL7', 'REGIONAL8', 'REGIONAL9', 'REGIONAL10', 'REGIONAL11']}
                                                    };     
                                                    return null;                     }                                                     
                                                },
                                                {headerName: 'AREA', field: 'AREA', width: 140, filterParams: {newRowsAction: 'keep'},
                                                cellEditorSelector:function (params){ 
                                                    if (params.data.AREA === 'AREA1' || params.data.AREA === 'AREA2' || params.data.AREA === 'AREA3' || params.data.AREA === 'AREA4' || params.data.AREA === "" ) return {
                                                        component: 'agRichSelectCellEditor',
                                                        params: {values: ['AREA1', 'AREA2','AREA3','AREA4']}
                                                    };   
                                                    return null;
                                                                                    }                                                 
                                                },
                                                {headerName: 'VENDOR', field: 'VENDOR', width: 140, filterParams: {newRowsAction: 'keep'},
                                                cellEditorSelector:function (params){    
                                                    if (params.data.VENDOR === 'ERICSSON' || params.data.VENDOR === 'HUAWEI' || params.data.VENDOR === 'NOKIA' || params.data.VENDOR === 'ZTE' || params.data.VENDOR === "" ) return {
                                                        component: 'agRichSelectCellEditor', 
                                                        params: {values: ['ERICSSON', 'HUAWEI','NOKIA','ZTE']} 
                                                    };    
                                                    return null;
                                                                                    }                                                  
                                                },
                                                {headerName: 'BSC Name', field: 'BSC_NAME', width: 140, filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'SITE NAME', field: 'SITE_NAME', width: 140, filterParams: {newRowsAction: 'keep'}}, 
                                                {headerName: 'LAC', field: 'LAC', width: 140, filterParams: {newRowsAction: 'keep'}}, 
                                                {headerName: 'CELL ID', field: 'CELL_ID', width: 140, filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'BTS NUMBER', field: 'BTSNUMBER', width: 140, filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'CELL NUMBER', field: 'CELLNUMBER', width: 140, filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'FREQUENCY', field: 'FREQUENCY', width: 140, filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'BANDTYPE', field: 'BANDTYPE', width: 140, filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'NCC', field: 'NCC', width: 140, filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'BCC', field: 'BCC', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F0', field: 'f0', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F1', field: 'f1', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F2', field: 'f2', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F3', field: 'f3', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F4', field: 'f4', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F5', field: 'f5', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F6', field: 'f6', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F7', field: 'f7', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F8', field: 'f8', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F9', field: 'f9', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F10', field: 'f10', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F11', field: 'f11', width: 140,filterParams: {newRowsAction: 'keep'}}, 
                                                {headerName: 'TRX SEC', field: 'Trx_Sec', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'TRX SITE', field: 'Trx_Site', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'JUMLAH SITE', field: 'JMLH_SITE', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'JUMLAH BTS', field: 'JMLH_BTS', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'JUMLAH CELL', field: 'JMLH_CELL', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'JUMLAH BSC', field: 'JMLH_BSC', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'METRO E', field: 'METRO_E', width: 140,filterParams: {newRowsAction: 'keep'},
                                                cellEditorSelector:function (params){  
                                                    if (params.data.METRO_E === 'YES' || params.data.METRO_E === 'NO' || params.data.METRO_E === "") return {
                                                        component: 'agRichSelectCellEditor',
                                                        params: {values: ['YES', 'NO']}
                                                    };     
                                                    return null;
                                                                                    }                                                
                                                },
                                                {headerName: 'OWNER LINK', field: 'OWNER_LINK', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'TIPE LINK', field: 'TIPE_LINK', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'FAR END LINK', field: 'FAR_END_LINK', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'TOTAL BANDWITH', field: 'TOTAL_BANDWITH', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'TANGGAL ONAIR LEASE LINE', field: 'TANGGAL_ONAIR_LEASE_LINE', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'SITE SIMPUL', field: 'SITE_SIMPUL', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'JUMLAH SITE UNDER SIMPUL', field: 'JUMLAH_SITE_UNDER_SIMPUL', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'STATUS LOKASI', field: 'STATUS_LOKASI', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'CLUSTER SALES', field: 'CLUSTER_SALES', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'TYPE BTS', field: 'TYPE_BTS', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'STATUS', field: 'STATUS', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'TYPE FREQ', field: 'TYPE_FREQ', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'NEW EXISTING', field: 'NEW_EXISTING', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'ONAIR', field: 'ONAIR', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'DATE ONAIR', field: 'DATE_ONAIR', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'KPI PASS', field: 'KPI_PASS', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'DATE KPI PASS', field: 'DATE_KPI_PASS', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'REMARK', field: 'REMARK', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'DEPARTEMENT', field: 'DEPARTEMENT', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'TECHNICAL AREA', field: 'TECHNICAL_AREA', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'LONGITUDE', field: 'LONGITUDE', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'LATITUDE', field: 'LATITUDE', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'ALAMAT', field: 'ALAMAT', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'KELURAHAN', field: 'KELURAHAN', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'KECAMATAN', field: 'KECAMATAN', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'KABUPATEN KOTA', field: 'KABUPATEN_KOTA', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'PROVINSI', field: 'PROVINSI', type: 'EditableColumn', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'TOWER PROVIDER', field: 'TOWER_PROVIDER', type: 'EditableColumn', width: 180,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'NAMA TOWER PROVIDER', field: 'NAMA_TOWER_PROVIDER', type: 'EditableColumn', width: 200,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'STATUS PLN', field: 'STATUS_PLN', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'VENDOR FMC', field: 'VENDOR_FMC', width: 250,filterParams: {newRowsAction: 'keep'}},
                                                ];  

                                                } else if(rule_user == "Power") {

                                                var columnDefs = [
                                                {headerName: 'CELL NAME', field: 'CELL_NAME', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'NE ID', field: 'NEID', width: 140, filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'SITE ID', field: 'SITEID', width: 140, filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'REGIONAL', field: 'REGIONAL', width: 140, suppressSizeToFit: true,filterParams: {newRowsAction: 'keep'},
                                                cellEditorSelector:function (params){  
                                                    if (params.data.REGIONAL === 'REGIONAL1' || params.data.REGIONAL === 'REGIONAL2' || params.data.REGIONAL === 'REGIONAL3' ||
                                                        params.data.REGIONAL === 'REGIONAL4' || params.data.REGIONAL === 'REGIONAL5' || params.data.REGIONAL === 'REGIONAL6' ||
                                                        params.data.REGIONAL === 'REGIONAL7' || params.data.REGIONAL === 'REGIONAL8' || params.data.REGIONAL === 'REGIONAL9' ||
                                                        params.data.REGIONAL === 'REGIONAL10' || params.data.REGIONAL === 'REGIONAL11' || params.data.REGIONAL === ""
                                                    ) return {
                                                        component: 'agRichSelectCellEditor', 
                                                        params: {values: ['REGIONAL1', 'REGIONAL2', 'REGIONAL3', 'REGIONAL4', 'REGIONAL5', 'REGIONAL6', 'REGIONAL7', 'REGIONAL8', 'REGIONAL9', 'REGIONAL10', 'REGIONAL11']}
                                                    };      
                                                    return null;                     }                                                     
                                                }, 
                                                {headerName: 'AREA', field: 'AREA', width: 140, filterParams: {newRowsAction: 'keep'},
                                                cellEditorSelector:function (params){ 
                                                    if (params.data.AREA === 'AREA1' || params.data.AREA === 'AREA2' || params.data.AREA === 'AREA3' || params.data.AREA === 'AREA4' || params.data.AREA === "" ) return {
                                                        component: 'agRichSelectCellEditor',
                                                        params: {values: ['AREA1', 'AREA2','AREA3','AREA4']}
                                                    };   
                                                    return null;
                                                                                    }                                                 
                                                },
                                                {headerName: 'VENDOR', field: 'VENDOR', width: 140, filterParams: {newRowsAction: 'keep'},
                                                cellEditorSelector:function (params){    
                                                    if (params.data.VENDOR === 'ERICSSON' || params.data.VENDOR === 'HUAWEI' || params.data.VENDOR === 'NOKIA' || params.data.VENDOR === 'ZTE' || params.data.VENDOR === "" ) return {
                                                        component: 'agRichSelectCellEditor', 
                                                        params: {values: ['ERICSSON', 'HUAWEI','NOKIA','ZTE']} 
                                                    };    
                                                    return null;
                                                                                    }                                                  
                                                },
                                                {headerName: 'BSC Name', field: 'BSC_NAME', width: 140, filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'SITE NAME', field: 'SITE_NAME', width: 140, filterParams: {newRowsAction: 'keep'}}, 
                                                {headerName: 'LAC', field: 'LAC', width: 140, filterParams: {newRowsAction: 'keep'}}, 
                                                {headerName: 'CELL ID', field: 'CELL_ID', width: 140, filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'BTS NUMBER', field: 'BTSNUMBER', width: 140, filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'CELL NUMBER', field: 'CELLNUMBER', width: 140, filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'FREQUENCY', field: 'FREQUENCY', width: 140, filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'BANDTYPE', field: 'BANDTYPE', width: 140, filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'NCC', field: 'NCC', width: 140, filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'BCC', field: 'BCC', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F0', field: 'f0', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F1', field: 'f1', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F2', field: 'f2', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F3', field: 'f3', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F4', field: 'f4', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F5', field: 'f5', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F6', field: 'f6', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F7', field: 'f7', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F8', field: 'f8', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F9', field: 'f9', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F10', field: 'f10', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'F11', field: 'f11', width: 140,filterParams: {newRowsAction: 'keep'}}, 
                                                {headerName: 'TRX SEC', field: 'Trx_Sec', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'TRX SITE', field: 'Trx_Site', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'JUMLAH SITE', field: 'JMLH_SITE', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'JUMLAH BTS', field: 'JMLH_BTS', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'JUMLAH CELL', field: 'JMLH_CELL', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'JUMLAH BSC', field: 'JMLH_BSC', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'METRO E', field: 'METRO_E', width: 140,filterParams: {newRowsAction: 'keep'},
                                                cellEditorSelector:function (params){  
                                                    if (params.data.METRO_E === 'YES' || params.data.METRO_E === 'NO' || params.data.METRO_E === "") return {
                                                        component: 'agRichSelectCellEditor',
                                                        params: {values: ['YES', 'NO']}
                                                    };     
                                                    return null;
                                                                                    }                                                
                                                },
                                                {headerName: 'OWNER LINK', field: 'OWNER_LINK', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'TIPE LINK', field: 'TIPE_LINK', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'FAR END LINK', field: 'FAR_END_LINK', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'TOTAL BANDWITH', field: 'TOTAL_BANDWITH', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'TANGGAL ONAIR LEASE LINE', field: 'TANGGAL_ONAIR_LEASE_LINE', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'SITE SIMPUL', field: 'SITE_SIMPUL', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'JUMLAH SITE UNDER SIMPUL', field: 'JUMLAH_SITE_UNDER_SIMPUL', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'STATUS LOKASI', field: 'STATUS_LOKASI', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'CLUSTER SALES', field: 'CLUSTER_SALES', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'TYPE BTS', field: 'TYPE_BTS', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'STATUS', field: 'STATUS', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'TYPE FREQ', field: 'TYPE_FREQ', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'NEW EXISTING', field: 'NEW_EXISTING', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'ONAIR', field: 'ONAIR', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'DATE ONAIR', field: 'DATE_ONAIR', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'KPI PASS', field: 'KPI_PASS', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'DATE KPI PASS', field: 'DATE_KPI_PASS', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'REMARK', field: 'REMARK', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'DEPARTEMENT', field: 'DEPARTEMENT', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'TECHNICAL AREA', field: 'TECHNICAL_AREA', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'LONGITUDE', field: 'LONGITUDE', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'LATITUDE', field: 'LATITUDE', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'ALAMAT', field: 'ALAMAT', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'KELURAHAN', field: 'KELURAHAN', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'KECAMATAN', field: 'KECAMATAN', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'KABUPATEN KOTA', field: 'KABUPATEN_KOTA', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'PROVINSI', field: 'PROVINSI', width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'TOWER PROVIDER', field: 'TOWER_PROVIDER', width: 180,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'NAMA TOWER PROVIDER', field: 'NAMA_TOWER_PROVIDER', width: 200,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'STATUS PLN', field: 'STATUS_PLN', type: 'EditableColumn',  width: 140,filterParams: {newRowsAction: 'keep'}},
                                                {headerName: 'VENDOR FMC', field: 'VENDOR_FMC', width: 250,filterParams: {newRowsAction: 'keep'}},
                                                ];   
 
                                                }

                                                function onGridSizeChanged(params) {
                                                    // get the current grids width
                                                    var gridWidth = document.getElementById('grid-wrapper').offsetWidth;
                                            
                                                    // keep track of which columns to hide/show
                                                    var columnsToShow = [];
                                                    var columnsToHide = [];
                                            
                                                    // iterate over all columns (visible or not) and work out
                                                    // now many columns can fit (based on their minWidth)
                                                    var totalColsWidth = 0;
                                                    var allColumns = params.columnApi.getAllColumns();
                                                    for (var i = 0; i < allColumns.length; i++) {
                                                        let column = allColumns[i];
                                                        totalColsWidth += column.getMinWidth();
                                                        if (totalColsWidth > gridWidth) {
                                                            columnsToHide.push(column.colId);
                                                        } else {
                                                            columnsToShow.push(column.colId);
                                                        }
                                                    }
                                            
                                                    // show/hide columns based on current grid width
                                                    params.columnApi.setColumnsVisible(columnsToShow, true);
                                                    params.columnApi.setColumnsVisible(columnsToHide, false);
                                            
                                                    // fill out any available space to ensure there are no gaps
                                               
                                            }
                                                 
                                                var gridOptions = {
                                                    defaultColDef: {
                                                        editable: false,
                                                        resizable: true,
                                                        filter: true 
                                                    },
                                                    columnTypes: { 
                                                        EditableColumn: {editable: true},
                                                    },

                                                    debug: true,
                                                    rowSelection: 'multiple',
                                                    paginationPageSize: limit,
                                                    columnDefs: columnDefs,      
                                                    // sideBar: true,
                                                    paginationAutoPageSize:true,
                                                    pagination: true,
                                                    // suppressPaginationPanel: true,
                                                    suppressScrollOnNewData: true, 
                                                    // onPaginationChanged: onPaginationChanged,
                                                    onSelectionChanged: onSelectionChanged,
                                                    onCellEditingStopped: onCellEditingStopped,
                                                    onGridSizeChanged: onGridSizeChanged
                                                };
                                                
                                                function setText(selector, text) {
                                                    document.querySelector(selector).innerHTML = text;
                                                }    
                                                 
                                                function onPaginationChanged() {
                                                    console.log('onPaginationPageLoaded');
                                                    
                                                    // Workaround for bug in events order
                                                    if (gridOptions.api) {
                                                        setText('#lbLastPageFound', gridOptions.api.paginationIsLastPageFound());
                                                        setText('#lbPageSize', gridOptions.api.paginationGetPageSize());
                                                        // we +1 to current page, as pages are zero based
                                                        setText('#lbCurrentPage', gridOptions.api.paginationGetCurrentPage() + 1);
                                                        setText('#lbTotalPages', gridOptions.api.paginationGetTotalPages());
                                                        
                                                        setLastButtonDisabled(!gridOptions.api.paginationIsLastPageFound());
                                                    } 
                                                } 
                                                
                                                function setLastButtonDisabled(disabled) {
                                                    document.querySelector('#btLast').disabled = disabled;
                                                }
                                                
                                                function setRowData(rowData) {
                                                    allOfTheData = rowData;
                                                    createNewDatasource();
                                                }
                                                
                                                function onSelectionChanged() {
                                                    var selectedRows = gridOptions.api.getSelectedRows();
                                                    var selectedRowsString = []; 
                                                    selectedRows.forEach( function(selectedRow, index) {
                                                        if (index!==0) {
                                                            selectedRowsString += ', ';
                                                        }
                                                        selectedRowsString.push(selectedRow);
                                                    });
                                                    return JSON.stringify({ data : selectedRowsString });
                                                }

                                                function onCellEditingStopped() {
                                                var selectedRows = gridOptions.api.getSelectedRows();

                                                console.log(selectedRows); 

                                                var selectedRowsString = [];
                                                selectedRows.forEach( function(selectedRow, index) {
                                                    if (index!==0) { 
                                                        selectedRowsString += ', ';
                                                    }
                                                    selectedRowsString.push(selectedRow);
                                                });
                                                datass = JSON.stringify({ data : selectedRowsString });
                                            
                                                console.log(datass);

                                                var data = [];
                                                    var httpRequest = new XMLHttpRequest();
                                                    var api = 'http://localhost/apis-btsonair/public/api/onair_bts_2g_update';
                                                    data = datass;
                                                    httpRequest.onload = function (data) {
                                                        console.log(data); 
                                                    };  
                                                    
                                                    httpRequest.open('PUT',api,true);
                                                    httpRequest.setRequestHeader("Content-type", "application/json; charset=utf-8");
                                                    httpRequest.send(data);
                                            } 
                                                
                                                document.addEventListener('keyup',function(event){
                                                    if (event.keyCode == 13) { 
                                                        var data = [];
                                                        var httpRequest = new XMLHttpRequest();
                                                        var api = 'http://localhost/apis-btsonair/public/api/onair_bts_2g_update';
                                                        data = onSelectionChanged();
                                                        httpRequest.onload = function (data) {
                                                            console.log(data);
                                                        }; 
                                                        
                                                        httpRequest.open('PUT',api,true);
                                                        httpRequest.setRequestHeader("Content-type", "application/json; charset=utf-8");
                                                        httpRequest.send(data);
                                                    } 
                                                }); 
                                                
                                                function onBtNext() {
                                                    id = 0; 
                                                    offset = limit;
                                                    limit += 50;
                                                    
                                                    // setup the grid after the page has finished loading
                                                    document.getElementById("btnNext").addEventListener('click', function() {
                                                        document.querySelector('#myGrid');
                                                        var httpRequest = new XMLHttpRequest(); 
                                                        
                                                        search = document.getElementById("search_data").value; 
                                                        var regional_user = <?php echo json_encode($regional_user); ?>;
                                                        if(search) { 
                                                        var api = encodeURI('http://localhost/apis-btsonair/public/api/onair_bts_2g?regional=' + regional_user + '&search=' + search + '&limit=' + limit + '&offset=' + offset);
                                                        } else { 
                                                        var api = encodeURI('http://localhost/apis-btsonair/public/api/onair_bts_2g?regional=' + regional_user + '&search=' + search + '&limit=' + limit + '&offset=' + offset);
                                                        } 
                                                        
                                                        httpRequest.open('GET', api);
                                                        httpRequest.send();
                                                        httpRequest.onreadystatechange = function() {
                                                            if (httpRequest.readyState === 4 && httpRequest.status === 200) {
                                                                var httpResponse = JSON.parse(httpRequest.responseText);
                                                                gridOptions.api.setRowData(httpResponse.data);
                                                            }
                                                        };
                                                    });
                                                     
                                                    gridOptions.api.paginationGoToNextPage();
                                                }
                                                
                                                function onBtPrevious() {
                                                    id = 0;
                                                    offset -= limit;
                                                    limit -= 50;
                                                    
                                                    // setup the grid after the page has finished loading
                                                    document.getElementById("btnPrevious").addEventListener('click', function() {
                                                        document.querySelector('#myGrid');
                                                        var httpRequest = new XMLHttpRequest();

                                                        search = document.getElementById("search_data").value; 
                                                        var regional_user = <?php echo json_encode($regional_user); ?>;
                                                        if(search) {
                                                        var api = encodeURI('http://localhost/apis-btsonair/public/api/onair_bts_2g?regional=' + regional_user + '&search=' + search + '&limit=' + limit + '&offset=' + offset);
                                                        } else {
                                                        var api = encodeURI('http://localhost/apis-btsonair/public/api/onair_bts_2g?regional=' + regional_user + '&search=' + search + '&limit=' + limit + '&offset=' + offset);
                                                        }
                                                        
                                                        httpRequest.open('GET', api);
                                                        httpRequest.send();
                                                        httpRequest.onreadystatechange = function() {
                                                            if (httpRequest.readyState === 4 && httpRequest.status === 200) {
                                                                var httpResponse = JSON.parse(httpRequest.responseText);
                                                                gridOptions.api.setRowData(httpResponse.data);
                                                            }
                                                        };
                                                    }); 
                                                     
                                                    gridOptions.api.paginationGoToPreviousPage();
                                                }

                                                function onBtnSearch() { 
                                                id = 0; 
                                                offset = limit;  
                                                limit += 50;   
                                                search = document.getElementById("search_data").value;
                                                console.log(search);   
                                                 
                                                // setup the grid after the page has finished loading
                                                document.getElementById("btn_search").addEventListener('click', function() {  
                                                    document.querySelector('#myGrid');  
                                                    var httpRequest = new XMLHttpRequest();
                                                    var regional_user = <?php echo json_encode($regional_user); ?>;
                                                    var api = encodeURI('http://localhost/apis-btsonair/public/api/onair_bts_2g?regional='+regional_user+'&search=' + search + '&limit=' + limit + '&offset=' + offset);
                                                    httpRequest.open('GET', api);     
                                                    httpRequest.send(); 
                                                    httpRequest.onreadystatechange = function() {
                                                        if (httpRequest.readyState === 4 && httpRequest.status === 200) {
                                                            var httpResponse = JSON.parse(httpRequest.responseText);
                                                            gridOptions.api.setRowData(httpResponse.data);
                                                        }
                                                    };     
                                                });   
                                                 }
                                                
                                                // setup the grid after the page has finished loading 
                                                document.addEventListener('DOMContentLoaded', function() {
                                                    // var search = document.getElementById("search_data").value;
                                                    var gridDiv = document.querySelector('#myGrid');
                                                    new agGrid.Grid(gridDiv, gridOptions); 
                                                    
                                                    // do http request to get our sample data - not using any framework to keep the example self contained.
                                                    // you will probably use a framework like JQuery, Angular or something else to do your HTTP calls.
                                                    var httpRequest = new XMLHttpRequest();
                                                    var regional_user = <?php echo json_encode($regional_user); ?>;
                                                    var api = encodeURI('http://localhost/apis-btsonair/public/api/onair_bts_2g?regional='+regional_user+'&offset=' + offset);
                                                    httpRequest.open('GET', api);
                                                    httpRequest.send(); 
                                                    httpRequest.onreadystatechange = function() {
                                                        if (httpRequest.readyState === 4 && httpRequest.status === 200) {
                                                            var httpResponse = JSON.parse(httpRequest.responseText);
                                                            gridOptions.api.setRowData(httpResponse.data);
                                                        }
                                                    }; 
                                                });  
                                            </script>
                                            
                                            
                                            
                                        </div>
                                    </div>
                                </div>
                            </div> 
                             
                            
                        </section><!-- SECTION 1 -->
                    </div><!-- CONTAINER-FLUID -->
                </section><!-- CONTENT -->
            </div><!-- CONTENT WRAPPER -->
        </div><!-- WRAPPER -->
    </div> <!-- BODOY -->
    
    <footer class="main-footer">
        <strong style="font-size: 12px">Copyright &copy; 2018 <a href="https://www.telkomsel.com">Telkomsel</a>.</strong>
    </footer>
    <!-- Control Sidebar -->
    <!-- <aside class="control-sidebar control-sidebar-dark">
    </aside> -->
    <!-- /.control-sidebar -->
    
    <!-- KUMPULAN SCRIPT  -->
    <!--  -->
    <script src="<?php echo e(url('')); ?>/plugins/jquery/jquery.min.js"></script>
    <script src="<?php echo e(url('')); ?>/plugins/datatables/jquery.dataTables.js"></script>
    <script src="<?php echo e(url('')); ?>/plugins/datatables/dataTables.bootstrap4.js"></script>
    <script src="<?php echo e(url('')); ?>/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo e(url('')); ?>/plugins/fastclick/fastclick.js"></script>
    <script src="<?php echo e(url('')); ?>/dist/js/adminlte.min.js"></script>
    <script src="<?php echo e(url('')); ?>/dist/js/demo.js"></script>
    <script src="<?php echo e(url('')); ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    
    <!-- Morris.js charts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="<?php echo e(url('')); ?>/plugins/morris/morris.min.js"></script>
    <!-- Sparkline -->
    <script src="<?php echo e(url('')); ?>/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="<?php echo e(url('')); ?>/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="<?php echo e(url('')); ?>/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="<?php echo e(url('')); ?>/plugins/knob/jquery.knob.js"></script>
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="<?php echo e(url('')); ?>/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="<?php echo e(url('')); ?>/plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?php echo e(url('')); ?>/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="<?php echo e(url('')); ?>/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo e(url('')); ?>/plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo e(url('')); ?>/dist/js/adminlte.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="<?php echo e(url('')); ?>/dist/js/pages/dashboard.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo e(url('')); ?>/dist/js/demo.js"></script>
    <script src="<?php echo e(url('')); ?>/plugins/chartjs-old/Chart.min.js"></script>
    
    <script type="text/javascript">
        function logout(){
            sessionStorage.remove('token', '');
            window.location.href = "<?php echo e(env('APP_URL')); ?>/landingPage/";
        }
    </script>
    <script src="<?php echo e(url('')); ?>/plugins/jquery/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script src="<?php echo e(url('')); ?>/plugins/jQueryUI/jquery-ui.min.js"></script>
    
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    
    <script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
    
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
    
    <script type="text/javascript">
    $(document).ready(function () {
        $('.navbar-light .dmenu').hover(function () {
            $(this).find('.sm-menu').first().stop(true, true).slideDown(150);
        }, function () {
            $(this).find('.sm-menu').first().stop(true, true).slideUp(105)
        });
    });
</script> 

    <script>
        
        $('#onairBts2g').DataTable( {
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url":"<?= route('getJsonBts2g') ?>",
                "dataType":"json",
                "type":"POST", 
                "data":{"_token":"<?= csrf_token() ?>"}
            }, 
            "columns":[
            {"data":"REGIONAL"},
            {"data":"AREA"},
            {"data":"VENDOR"},
            {"data":"BSC_NAME"},
            {"data":"SITE_NAME"},
            {"data":"CELL_NAME"},
            {"data":"NEID"},
            {"data":"SITEID"},
            {"data":"LAC"},
            {"data":"CELL_ID"},
            {"data":"BTSNUMBER"},
            {"data":"CELLNUMBER"},
            {"data":"FREQUENCY"},
            {"data":"BANDTYPE"},
            {"data":"NCC"}, 
            {"data":"BCC"},
            {"data":"f0"},
            {"data":"f1"},
            {"data":"f2"},
            {"data":"f3"},
            {"data":"f4"},
            {"data":"f5"}, 
            {"data":"f6"},
            {"data":"f7"},
            {"data":"f8"},
            {"data":"f9"},
            {"data":"f10"},
            {"data":"f11"},
            {"data":"Trx_Sec"},
            {"data":"Trx_Site"},
            {"data":"JMLH_SITE"},
            {"data":"JMLH_BTS"},
            {"data":"JMLH_CELL"},
            {"data":"JMLH_BSC"},
            {"data":"METRO_E"},
            {"data":"OWNER_LINK"},
            {"data":"TIPE_LINK"},
            {"data":"FAR_END_LINK"},
            {"data":"TOTAL_BANDWITH"},
            {"data":"TANGGAL_ONAIR_LEASE_LINE"},
            {"data":"SITE_SIMPUL"},
            {"data":"JUMLAH_SITE_UNDER_SIMPUL"},
            {"data":"STATUS_LOKASI"},
            {"data":"CLUSTER_SALES"},
            {"data":"TYPE_BTS"},
            {"data":"STATUS"},
            {"data":"TYPE_FREQ"},
            {"data":"NEW_EXISTING"},                        
            {"data":"ONAIR"},
            {"data":"DATE_ONAIR"},
            {"data":"KPI_PASS"},
            {"data":"DATE_KPI_PASS"},
            {"data":"REMARK"},
            {"data":"DEPARTEMENT"},
            {"data":"TECHNICAL_AREA"},
            {"data":"ALAMAT"},
            {"data":"KELURAHAN"},
            {"data":"KECAMATAN"},
            {"data":"KABUPATEN_KOTA"},
            {"data":"PROVINSI"},
            {"data":"TOWER_PROVIDER"},
            {"data":"NAMA_TOWER_PROVIDER"},
            {"data":"STATUS_PLN"},
            {"data":"VENDOR_FMC"},
            {"data":"INPUT_DATE"},
            {"data":"STATUS_BTS"}
            ],
            "pageLength": 10,
            "scrollX": true,
            columnDefs: [
            { width: 600, targets: 55 },
            { width: 300, targets: 63 }
            ],
            fixedColumns: true
        });  
    </script>
    
    <script>
        // *** START MONTH AND YEAR -1
        let listMonthLong = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
        let dt = new Date();
        let m = dt.getMonth()
        let n = dt.getMonth() - 1;
        let currYear  = dt.getFullYear();
        if (n < 0)
        {
            n = 11;
            currYear = currYear - 1;
        }
        document.getElementById("bulan").innerHTML = listMonthLong[n];
        document.getElementById("tahun").innerHTML = currYear.toString();
        document.getElementById("bulan2").innerHTML = listMonthLong[n];
        document.getElementById("tahun2").innerHTML = currYear.toString();
        document.getElementById("bulan3").innerHTML = listMonthLong[m];
        // *** END MONTH AND YEAR -1
        function dropDead() {
            document.getElementById("dropIcon").classList.toggle('putar');
            document.getElementById("dropId").classList.toggle("show");
        }
        $("#nav-open").click(function(){
            if (document.getElementById("wrapper").style.padding == "0px") {
                $("#wrapper").css('padding-left', '150px');
                $("#sidebar-wrapper").css('width', '150px');
            }else {
                document.getElementById("wrapper").style.padding = "0";
                document.getElementById("sidebar-wrapper").style.width = "0";
            }
        });
        function formatNumber(num) {
            return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
        }
        $(document).ready(function(){
            //
            // $.ajax({
                //     url:"http://10.54.36.49/bts-rekon/api/get_summary_all",
                //     type:"get",
                //     success: function(result){
                    //         var hasil = JSON.parse(result);
                    
                    //         $('#total_on_2g').html(hasil.total_on_2g.toLocaleString('en'));
                    //         $('#new_2g').html(hasil.new_2g);
                    //         $('#existing_2g').html(hasil.existing_2g);
                    //         $('#dismantle_2g').html(hasil.dismantle_2g);
                    //         $('#total_on_3g').html(hasil.total_on_3g.toLocaleString('en'));
                    //         $('#new_3g').html(hasil.new_3g);
                    //         $('#existing_3g').html(hasil.existing_3g);
                    //         $('#dismantle_3g').html(hasil.dismantle_3g);
                    //         $('#total_on_4g').html(hasil.total_on_4g.toLocaleString('en'));
                    //         $('#new_4g').html(hasil.new_4g);
                    //         $('#existing_4g').html(hasil.existing_4g);
                    //         $('#dismantle_4g').html(hasil.dismantle_4g);
                    //         $('#total_on_total').html(hasil.total_on_total.toLocaleString('en'));
                    //         $('#new_total').html(hasil.new_total);
                    //         $('#existing_total').html(hasil.existing_total);
                    //         $('#dismantle_total').html(hasil.dismantle_total);
                    //         // console.log(hasil);
                    //     }
                    // });
                    //
                    // *** START ON AIR BOX ***
                    // 2G
                    $.ajax({
                        url:"http://localhost/apis-btsonair/public/api/dashboard_2g_monthly",
                        type:"get",
                        success: function(result){
                            $('#onair_2g').html(result.onair_2g);
                            $('#new_2g').html(result.new_2g);
                            $('#dismantle_2g').html(result.dismantle_2g);
                        }
                    });
                    
                    // 3G
                    $.ajax({
                        url:"http://localhost/apis-btsonair/public/api/dashboard_3g_monthly",
                        type:"get",
                        success: function(result){
                            $('#onair_3g').html(result.onair_3g);
                            $('#new_3g').html(result.new_3g);
                            $('#dismantle_3g').html(result.dismantle_3g);
                        }
                    });
                    
                    // 4G
                    $.ajax({
                        url:"http://localhost/apis-btsonair/public/api/dashboard_4g_monthly",
                        type:"get",
                        success: function(result){
                            $('#onair_4g').html(result.onair_4g);
                            $('#new_4g').html(result.new_4g);
                            $('#dismantle_4g').html(result.dismantle_4g);
                        }
                    });
                    
                    // TOTAL
                    $.ajax({
                        url:"http://localhost/apis-btsonair/public/api/dashboard_total_monthly",
                        type:"get",
                        success: function(result){
                            $('#onair_total').html(result.onair_total);
                            $('#new_total').html(result.new_total);
                            $('#dismantle_total').html(result.dismantle_total);
                        }
                    });
                    
                    // *** END ON AIR BOX ***
                    
                    // *** START TABLE BTS SUMMARY ***
                    $.ajax({
                        url:"http://localhost/apis-btsonair/public/api/bts_summary_2_monthly",
                        type:"get",
                        success: function(result){
                            $('#resume_sysinfo').empty();
                            for (var i = 0; i < result.length-1; i++) {
                                // console.log(result[i].regional);
                                $('#resume_sysinfo').append('<tr><td>'+result[i].regional+'</td><td>'+result[i].data1+'</td><td>'+result[i].data2+'</td><td>'+result[i].data3+'</td><td>'+result[i].data4+'</td><td>'+result[i].data5+'</td></tr>');
                            }
                        }
                    });
                    
                    $.ajax({
                        url:"http://localhost/apis-btsonair/public/api/bts_summary_2_monthly",
                        type:"get",
                        success: function(result){
                            $('#resume_sysinfo_total').empty();
                            for (var i = 11; i < result.length; i++) {
                                // console.log(result[i].regional);
                                $('#resume_sysinfo_total').append('<td style="text-transform: uppercase;">'+result[i].regional+'</td><td>'+result[i].data1+'</td><td>'+result[i].data2+'</td><td>'+result[i].data3+'</td><td>'+result[i].data4+'</td><td>'+result[i].data5+'</td>');
                            }
                        }
                    });
                    // *** END TABLE BTS SUMMARY ***
                    
                    // *** START TABLE RESUME SITE ***
                    $.ajax({
                        url:"http://localhost/apis-btsonair/public/api/resume_site_monthly",
                        type:"get",
                        success: function(result){
                            // console.log("result", result);
                            $('#resume_site').empty();
                            for (var i = 0; i < result.length-1; i++) {
                                // console.log(result[i].regional);
                                $('#resume_site').append('<tr><td style="text-transform: uppercase;">'+result[i].regional+'</td><td>'+result[i].site_2g3g+'</td><td>'+result[i].site_2g3g4g+'</td><td>'+result[i].site_2g4g+'</td><td>'+result[i].site_2g+'</td><td>'+result[i].site_3g4g+'</td><td>'+result[i].site_3g+'</td><td>'+result[i].site_4g+'</td><td>'+result[i].total+'</td></tr>');
                            }
                        }
                    });
                    
                    $.ajax({
                        url:"http://localhost/apis-btsonair/public/api/resume_site_monthly",
                        type:"get",
                        success: function(result){
                            $('#resume_site_total').empty();
                            for (var i = result.length-1; i < result.length; i++) {
                                // console.log(result[i].regional);
                                $('#resume_site_total').append('<td style="text-transform: uppercase;">'+result[i].regional+'</td><td>'+result[i].site_2g3g+'</td><td>'+result[i].site_2g3g4g+'</td><td>'+result[i].site_2g4g+'</td><td>'+result[i].site_2g+'</td><td>'+result[i].site_3g4g+'</td><td>'+result[i].site_3g+'</td><td>'+result[i].site_4g+'</td><td>'+result[i].total+'</td>');
                            }
                        }
                    });
                    // *** END TABLE RESUME SITE ***
                    
                    // *** START TABLE RESUME NE ***
                    $.ajax({
                        url:"http://localhost/apis-btsonair/public/api/resume_ne_monthly",
                        type:"get",
                        success: function(result){
                            $('#resume_ne').empty();
                            for (var i = 0; i < result.length-1; i++) {
                                // console.log(result[i].regional);
                                $('#resume_ne').append('<tr><td colspan="2">'+result[i].regional+'</td><td colspan="2">'+result[i].vendor+'</td><td>'+result[i].bsc+'</td><td>'+result[i].site+'</td><td>'+result[i].bts+'</td><td>'+result[i].sec2g+'</td><td>'+result[i].trx+'</td><td>'+result[i].rnc+'</td><td>'+result[i].f1f2+'</td><td>'+result[i].sec6+'</td><td>'+result[i].f3+'</td><td>'+result[i].btshtl+'</td><td>'+result[i].sec3g+'</td><td>'+result[i].enodeb+'</td><td>'+result[i].sec4g+'</td></tr>');
                            }
                        }
                    });
                    $.ajax({
                        url:"http://localhost/apis-btsonair/public/api/resume_ne_monthly",
                        type:"get",
                        success: function(result){
                            $('#resume_ne_total').empty();
                            for (var i = 16; i < result.length; i++) {
                                // console.log(result[i].regional);
                                $('#resume_ne_total').append('<td colspan="4"style="text-align:center;vertical-align:middle"> TOTAL </td><td class="kepanjangantot">'+result[i].bsc+'</td><td class="kepanjangantot">'+result[i].site+'</td><td class="kepanjangantot">'+result[i].bts+'</td><td class="kepanjangantot">'+result[i].sec2g+'</td><td class="kepanjangantot">'+result[i].trx+'</td><td class="kepanjangantot">'+result[i].rnc+'</td><td class="kepanjangantot">'+result[i].f1f2+'</td><td class="kepanjangantot">'+result[i].sec6+'</td><td class="kepanjangantot">'+result[i].f3+'</td><td class="kepanjangantot">'+result[i].btshtl+'</td><td class="kepanjangantot">'+result[i].sec3g+'</td><td class="kepanjangantot">'+result[i].enodeb+'</td><td class="kepanjangantot">'+result[i].sec4g+'</td>');
                            }
                        }
                    });
                    // *** END TABLE RESUME NE ***
                    
                    // *** START TABLE RESUME NODIN BTS ***
                    $.ajax({
                        url:"http://localhost/apis-btsonair/public/api/bts_summary_1",
                        type:"get",
                        success: function(result){
                            
                            $('#resume_nodin_bts_tanggal').append('<td colspan="2" style="text-transform: uppercase;">Radio Network Capacity Built (Unit)</td>');
                            for (var i = 0; i < result.length; i++) {
                                
                                const monthNames = ["January", "February", "March", "April", "May", "June",
                                "July", "August", "September", "October", "November", "December"
                                ];
                                
                                var d = new Date(result[i].date),
                                month = monthNames[d.getMonth()],
                                year = d.getFullYear();
                                
                                var b = month +" " + year;
                                
                                $('#resume_nodin_bts_tanggal').append('<td>'+b+'</td>');
                            }
                            
                            $('#resume_nodin_bts_2g_onair').append('<td colspan="2">BTS 2G - On Air This Year</td>');
                            for (var i = 0; i < result.length; i++) {
                                $('#resume_nodin_bts_2g_onair').append('<td style="text-align:center;vertical-align:middle">'+result[i].onair_2g+'</td>');
                            }
                            
                            $('#resume_nodin_bts_2g_dismantled').append('<td colspan="2">BTS 2G - Dismantled</td>');
                            for (var i = 0; i < result.length; i++) {
                                $('#resume_nodin_bts_2g_dismantled').append('<td style="text-align:center;vertical-align:middle"> '+result[i].offair_2g+'</td>');
                            }
                            
                            $('#resume_nodin_bts_2g_total').append('<td colspan="2">BTS 2G - Total On Air</td>');
                            for (var i = 0; i < result.length; i++) {
                                $('#resume_nodin_bts_2g_total').append('<td style="text-align:center;vertical-align:middle">'+result[i].total_2g+'</td>');
                            }
                            
                            $('#resume_nodin_bts_3g_onair').append('<td colspan="2">BTS 3G - On this year</td>');
                            for (var i = 0; i < result.length; i++) {
                                $('#resume_nodin_bts_3g_onair').append('<td style="text-align:center;vertical-align:middle">'+result[i].onair_3g+'</td>');
                            }
                            
                            $('#resume_nodin_bts_3g_dismantled').append('<td colspan="2">BTS 3G - Dismantledr</td>');
                            for (var i = 0; i < result.length; i++) {
                                $('#resume_nodin_bts_3g_dismantled').append('<td style="text-align:center;vertical-align:middle">'+result[i].offair_3g+'</td>');
                            }
                            
                            $('#resume_nodin_bts_3g_total').append('<td colspan="2">BTS 3G - Total On Air</td>');
                            for (var i = 0; i < result.length; i++) {
                                $('#resume_nodin_bts_3g_total').append('<td style="text-align:center;vertical-align:middle">'+result[i].total_3g+'</td>');
                            }
                            
                            $('#resume_nodin_bts_4g_onair').append('<td colspan="2">BTS 4G - On this year</td>');
                            for (var i = 0; i < result.length; i++) {
                                $('#resume_nodin_bts_4g_onair').append('<td style="text-align:center;vertical-align:middle">'+result[i].onair_4g+'</td>');
                            }
                            
                            $('#resume_nodin_bts_4g_dismantled').append('<td colspan="2">BTS 4G - Dismantled</td>');
                            for (var i = 0; i < result.length; i++) {
                                $('#resume_nodin_bts_4g_dismantled').append('<td style="text-align:center;vertical-align:middle">'+result[i].offair_4g+'</td>');
                            }
                            
                            $('#resume_nodin_bts_4g_total').append('<td colspan="2">BTS 4G - Total On Air</td>');
                            for (var i = 0; i < result.length; i++) {
                                $('#resume_nodin_bts_4g_total').append('<td style="text-align:center;vertical-align:middle">'+result[i].total_4g+'</td>');
                            }
                            
                            $('#resume_nodin_trx_onair').append('<td colspan="2">TRX - On Air this year</td>');
                            for (var i = 0; i < result.length; i++) {
                                $('#resume_nodin_trx_onair').append('<td style="text-align:center;vertical-align:middle">'+result[i].onair_trx+'</td>');
                            }
                            
                            $('#resume_nodin_trx_dismantled').append('<td colspan="2">TRX - Dismantled</td>');
                            for (var i = 0; i < result.length; i++) {
                                $('#resume_nodin_trx_dismantled').append('<td style="text-align:center;vertical-align:middle">'+result[i].offair_trx+'</td>');
                            }
                            
                            $('#resume_nodin_trx_total').append('<td colspan="2">TRX - Total On Air</td>');
                            for (var i = 0; i < result.length; i++) {
                                $('#resume_nodin_trx_total').append('<td style="text-align:center;vertical-align:middle">'+result[i].total_trx+'</td>');
                            }
                            
                            $('#resume_nodin_jumlah_bts').append('<td colspan="2">Jumlah BTS (2G, 3G & 4G)</td>');
                            for (var i = 0; i < result.length; i++) {
                                $('#resume_nodin_jumlah_bts').append('<td style="text-align:center;vertical-align:middle">'+result[i].total_all+'</td>');
                            }
                            
                            $('#resume_nodin_jumlah_site_2g').append('<td colspan="2">Jumlah Site 2G Only</td>');
                            for (var i = 0; i < result.length; i++) {
                                $('#resume_nodin_jumlah_site_2g').append('<td style="text-align:center;vertical-align:middle">'+result[i].only_2g+'</td>');
                            }
                            
                            $('#resume_nodin_jumlah_site_3g').append('<td colspan="2">Jumlah Site 3G Only</td>');
                            for (var i = 0; i < result.length; i++) {
                                $('#resume_nodin_jumlah_site_3g').append('<td style="text-align:center;vertical-align:middle">'+result[i].only_3g+'</td>');
                            }
                            
                            $('#resume_nodin_jumlah_site_4g').append('<td colspan="2">Jumlah Site 4G Only</td>');
                            for (var i = 0; i < result.length; i++) {
                                $('#resume_nodin_jumlah_site_4g').append('<td style="text-align:center;vertical-align:middle">'+result[i].only_4g+'</td>');
                            }
                            
                            $('#resume_nodin_total_site').append('<td colspan="2">Jumlah Site 2G 3G & 4G</td>');
                            for (var i = 0; i < result.length; i++) {
                                $('#resume_nodin_total_site').append('<td style="text-align:center;vertical-align:middle">'+result[i].collocation+'</td>');
                            }
                            
                            $('#resume_nodin_total_site_keseluruhan').append('<td colspan="2">Total Jumlah Site</td>');
                            for (var i = 0; i < result.length; i++) {
                                $('#resume_nodin_total_site_keseluruhan').append('<td style="text-align:center;vertical-align:middle">'+result[i].total_all_site+'</td>');
                            }
                        }
                    }); 
                    // *** END TABLE RESUME NODIN BTS ***
                    
                    
                });
            </script>
        </body>
        </html>
        <?php /**PATH D:\software\xampp\htdocs\dashboard-bts-on-air\resources\views/baseline-bts2g.blade.php ENDPATH**/ ?>